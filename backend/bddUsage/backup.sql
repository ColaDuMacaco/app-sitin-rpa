/*
PostgreSQL Backup
Database: rpa/public
Backup Time: 2021-09-16 02:27:42
*/

DROP SEQUENCE IF EXISTS "public"."calificacion_id_calificacion_seq";
DROP SEQUENCE IF EXISTS "public"."entidad_id_entidad_seq";
DROP SEQUENCE IF EXISTS "public"."log_cliente_id_log_cliente_seq";
DROP SEQUENCE IF EXISTS "public"."log_rpa_id_log_rpa_seq";
DROP SEQUENCE IF EXISTS "public"."programa_id_programa_seq";
DROP SEQUENCE IF EXISTS "public"."programa_id_region_seq";
DROP SEQUENCE IF EXISTS "public"."transaccion_id_transaccion_seq";
DROP SEQUENCE IF EXISTS "public"."usuario_entidad_id_usuario_entidad_seq";
DROP TABLE IF EXISTS "public"."calificacion";
DROP TABLE IF EXISTS "public"."cliente";
DROP TABLE IF EXISTS "public"."entidad";
DROP TABLE IF EXISTS "public"."log_cliente";
DROP TABLE IF EXISTS "public"."log_rpa";
DROP TABLE IF EXISTS "public"."programa";
DROP TABLE IF EXISTS "public"."transaccion";
DROP TABLE IF EXISTS "public"."usuario_entidad";
CREATE SEQUENCE "calificacion_id_calificacion_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "entidad_id_entidad_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "log_cliente_id_log_cliente_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "log_rpa_id_log_rpa_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "programa_id_programa_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "programa_id_region_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "transaccion_id_transaccion_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "usuario_entidad_id_usuario_entidad_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "calificacion" (
  "id_calificacion" int4 NOT NULL DEFAULT nextval('calificacion_id_calificacion_seq'::regclass),
  "id_programa" int8,
  "tipo_programa" varchar(10) COLLATE "pg_catalog"."default",
  "nombre_programa" text COLLATE "pg_catalog"."default",
  "fecha_inicio_programa" date,
  "fecha_termino_programa" date,
  "rut_cliente" varchar(10) COLLATE "pg_catalog"."default",
  "rut_empresa_cliente" varchar(10) COLLATE "pg_catalog"."default",
  "nombre_empresa_cliente" varchar(25) COLLATE "pg_catalog"."default",
  "nombre_region" varchar(40) COLLATE "pg_catalog"."default",
  "id_entidad" int8,
  "vigencia" bool
)
;
ALTER TABLE "calificacion" OWNER TO "postgres";
CREATE TABLE "cliente" (
  "rut_cliente" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "nombre_cliente" varchar COLLATE "pg_catalog"."default",
  "apellido_cliente" varchar COLLATE "pg_catalog"."default",
  "correo_cliente" varchar COLLATE "pg_catalog"."default",
  "clave_cliente" varchar(20) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "cliente" OWNER TO "postgres";
CREATE TABLE "entidad" (
  "id_entidad" int4 NOT NULL DEFAULT nextval('entidad_id_entidad_seq'::regclass),
  "nombre_entidad" varchar(10) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "entidad" OWNER TO "postgres";
CREATE TABLE "log_cliente" (
  "id_log_cliente" int4 NOT NULL DEFAULT nextval('log_cliente_id_log_cliente_seq'::regclass),
  "rut_cliente" varchar(10) COLLATE "pg_catalog"."default",
  "id_transaccion" int4,
  "hora_transaccion" timestamp(6),
  "descripcion_transaccion" text COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "log_cliente" OWNER TO "postgres";
CREATE TABLE "log_rpa" (
  "id_log_rpa" int4 NOT NULL DEFAULT nextval('log_rpa_id_log_rpa_seq'::regclass),
  "id_transaccion" int4,
  "hora_inicio_transaccion" timestamp(6),
  "hora_final_transaccion" timestamp(6),
  "descripcion_transaccion" text COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "log_rpa" OWNER TO "postgres";
CREATE TABLE "programa" (
  "id_programa" int4 NOT NULL DEFAULT nextval('programa_id_programa_seq'::regclass),
  "tipo_programa" varchar COLLATE "pg_catalog"."default",
  "nombre_programa" text COLLATE "pg_catalog"."default",
  "fecha_inicio_programa" date,
  "fecha_termino_programa" date,
  "monto_min_programa" int8,
  "monto_max_programa" int8,
  "id_region" int4 NOT NULL DEFAULT nextval('programa_id_region_seq'::regclass),
  "nombre_region" varchar(40) COLLATE "pg_catalog"."default",
  "id_entidad" int8
)
;
ALTER TABLE "programa" OWNER TO "postgres";
CREATE TABLE "transaccion" (
  "id_transaccion" int4 NOT NULL DEFAULT nextval('transaccion_id_transaccion_seq'::regclass),
  "tipo_transaccion" varchar COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "transaccion" OWNER TO "postgres";
CREATE TABLE "usuario_entidad" (
  "id_usuario_entidad" int4 NOT NULL DEFAULT nextval('usuario_entidad_id_usuario_entidad_seq'::regclass),
  "rut_cliente" varchar COLLATE "pg_catalog"."default",
  "clave_entidad" varchar COLLATE "pg_catalog"."default",
  "id_entidad" int4
)
;
ALTER TABLE "usuario_entidad" OWNER TO "postgres";
BEGIN;
LOCK TABLE "public"."calificacion" IN SHARE MODE;
DELETE FROM "public"."calificacion";
COMMIT;
BEGIN;
LOCK TABLE "public"."cliente" IN SHARE MODE;
DELETE FROM "public"."cliente";
INSERT INTO "public"."cliente" ("rut_cliente","nombre_cliente","apellido_cliente","correo_cliente","clave_cliente") VALUES ('9214798-3', 'pedrito', 'lopez', 'nose@gmail.com', '12345');
COMMIT;
BEGIN;
LOCK TABLE "public"."entidad" IN SHARE MODE;
DELETE FROM "public"."entidad";
INSERT INTO "public"."entidad" ("id_entidad","nombre_entidad") VALUES (1, 'Sercotec');
COMMIT;
BEGIN;
LOCK TABLE "public"."log_cliente" IN SHARE MODE;
DELETE FROM "public"."log_cliente";
COMMIT;
BEGIN;
LOCK TABLE "public"."log_rpa" IN SHARE MODE;
DELETE FROM "public"."log_rpa";
COMMIT;
BEGIN;
LOCK TABLE "public"."programa" IN SHARE MODE;
DELETE FROM "public"."programa";
COMMIT;
BEGIN;
LOCK TABLE "public"."transaccion" IN SHARE MODE;
DELETE FROM "public"."transaccion";
COMMIT;
BEGIN;
LOCK TABLE "public"."usuario_entidad" IN SHARE MODE;
DELETE FROM "public"."usuario_entidad";
INSERT INTO "public"."usuario_entidad" ("id_usuario_entidad","rut_cliente","clave_entidad","id_entidad") VALUES (1, '9214798-3', '2qa1ws', 1);
COMMIT;
ALTER TABLE "calificacion" ADD CONSTRAINT "calificacion_pkey" PRIMARY KEY ("id_calificacion");
ALTER TABLE "cliente" ADD CONSTRAINT "cliente_pkey" PRIMARY KEY ("rut_cliente");
ALTER TABLE "entidad" ADD CONSTRAINT "entidad_pkey" PRIMARY KEY ("id_entidad");
ALTER TABLE "log_cliente" ADD CONSTRAINT "log_cliente_pkey" PRIMARY KEY ("id_log_cliente");
ALTER TABLE "log_rpa" ADD CONSTRAINT "log_rpa_pkey" PRIMARY KEY ("id_log_rpa");
ALTER TABLE "programa" ADD CONSTRAINT "programa_pkey" PRIMARY KEY ("id_programa");
ALTER TABLE "transaccion" ADD CONSTRAINT "transaccion_pkey" PRIMARY KEY ("id_transaccion");
ALTER TABLE "usuario_entidad" ADD CONSTRAINT "pk_usuario_entidad" PRIMARY KEY ("id_usuario_entidad");
ALTER TABLE "calificacion" ADD CONSTRAINT "fk_id_programa" FOREIGN KEY ("id_programa") REFERENCES "public"."programa" ("id_programa") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "log_cliente" ADD CONSTRAINT "unique_id_transaccion" UNIQUE ("id_transaccion");
ALTER TABLE "log_cliente" ADD CONSTRAINT "fk_rut_cliente" FOREIGN KEY ("rut_cliente") REFERENCES "public"."cliente" ("rut_cliente") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "log_rpa" ADD CONSTRAINT "fk_id_transaccion" FOREIGN KEY ("id_transaccion") REFERENCES "public"."transaccion" ("id_transaccion") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "programa" ADD CONSTRAINT "fk_id_entidad" FOREIGN KEY ("id_entidad") REFERENCES "public"."entidad" ("id_entidad") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "transaccion" ADD CONSTRAINT "fk_id_transaccion" FOREIGN KEY ("id_transaccion") REFERENCES "public"."log_cliente" ("id_transaccion") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "usuario_entidad" ADD CONSTRAINT "fk_cliente" FOREIGN KEY ("rut_cliente") REFERENCES "public"."cliente" ("rut_cliente") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "usuario_entidad" ADD CONSTRAINT "fk_usuario_entidad" FOREIGN KEY ("id_entidad") REFERENCES "public"."entidad" ("id_entidad") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER SEQUENCE "calificacion_id_calificacion_seq"
OWNED BY "calificacion"."id_calificacion";
SELECT setval('"calificacion_id_calificacion_seq"', 3, false);
ALTER SEQUENCE "calificacion_id_calificacion_seq" OWNER TO "postgres";
ALTER SEQUENCE "entidad_id_entidad_seq"
OWNED BY "entidad"."id_entidad";
SELECT setval('"entidad_id_entidad_seq"', 3, true);
ALTER SEQUENCE "entidad_id_entidad_seq" OWNER TO "postgres";
ALTER SEQUENCE "log_cliente_id_log_cliente_seq"
OWNED BY "log_cliente"."id_log_cliente";
SELECT setval('"log_cliente_id_log_cliente_seq"', 3, false);
ALTER SEQUENCE "log_cliente_id_log_cliente_seq" OWNER TO "postgres";
ALTER SEQUENCE "log_rpa_id_log_rpa_seq"
OWNED BY "log_rpa"."id_log_rpa";
SELECT setval('"log_rpa_id_log_rpa_seq"', 3, false);
ALTER SEQUENCE "log_rpa_id_log_rpa_seq" OWNER TO "postgres";
ALTER SEQUENCE "programa_id_programa_seq"
OWNED BY "programa"."id_programa";
SELECT setval('"programa_id_programa_seq"', 3, false);
ALTER SEQUENCE "programa_id_programa_seq" OWNER TO "postgres";
ALTER SEQUENCE "programa_id_region_seq"
OWNED BY "programa"."id_region";
SELECT setval('"programa_id_region_seq"', 3, false);
ALTER SEQUENCE "programa_id_region_seq" OWNER TO "postgres";
ALTER SEQUENCE "transaccion_id_transaccion_seq"
OWNED BY "transaccion"."id_transaccion";
SELECT setval('"transaccion_id_transaccion_seq"', 3, false);
ALTER SEQUENCE "transaccion_id_transaccion_seq" OWNER TO "postgres";
ALTER SEQUENCE "usuario_entidad_id_usuario_entidad_seq"
OWNED BY "usuario_entidad"."id_usuario_entidad";
SELECT setval('"usuario_entidad_id_usuario_entidad_seq"', 3, false);
ALTER SEQUENCE "usuario_entidad_id_usuario_entidad_seq" OWNER TO "postgres";
