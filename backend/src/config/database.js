/* IMPORT */
//const dotenv = require('dotenv').config();
const Pool = require('pg').Pool


/* ENVS */
//dotenv.config()
const poolrpa = new Pool ({
    host: process.env.HOST_RPA,
    user: process.env.USER_DB_RPA,
    password: process.env.PASS_DB_RPA,
    database: process.env.DATABASE_RPA,
    port: process.env.PORT_RPA
});



module.exports = {
    poolrpa,
};