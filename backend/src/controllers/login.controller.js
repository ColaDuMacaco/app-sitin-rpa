const { json } = require('express');
const jwt = require('jsonwebtoken');
const { poolrpa } = require('../config/database')
const bcrypt = require('bcrypt');

var Fn = {
	// Valida el rut con su cadena completa "XXXXXXXX-X"
	validaRut : function (rutCompleto) {
		if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( rutCompleto ))
			return false;
		var tmp 	= rutCompleto.split('-');
		var digv	= tmp[1]; 
		var rut 	= tmp[0];
		if ( digv == 'K' ) digv = 'k' ;
		return (Fn.dv(rut) == digv );
	},
	dv : function(T){
		var M=0,S=1;
		for(;T;T=Math.floor(T/10))
			S=(S+T%10*(9-M++%6))%11;
		return S?S-1:'k';
	}
}

const login = async (req,res) => {
    console.log("entrando a funcion de login")
    res.header("Access-Control-Allow-Origin","*");
    const { rut_cliente,clave_cliente } = req.body;
    //const hash = bcrypt.hashSync(clave_cliente, 5);
    const sqlQuery = `SELECT clave_cliente FROM Cliente WHERE rut_cliente = $1;`
    const values = [rut_cliente];
    const response = await poolrpa.query(sqlQuery, values);
    if(response.rows.length == 0 ){
        res.json({
            status:-1
        });
        return;
    }
    console.log(response.rows[0].clave_cliente)
    let compare = bcrypt.compareSync(clave_cliente,response.rows[0].clave_cliente)
    console.log(compare)
    if(compare == true && Fn.validaRut(rut_cliente) == true){
        const user = {
            rut_cliente,
            clave_cliente
        }
        jwt.sign({ user },'llavesecreta', (err,token) => {
            res.json({
                status: 1,
                token
            })
        })
    }else if(compare == false || Fn.validaRut(rut_cliente) == false ){
        res.json({
            status:-1
        });
    }
    
    /*let compare = bcrypt.compareSync(clave_cliente,hash)
    
    console.log(compare)
    if (response.rows[0].count == '1' && Fn.validaRut(rut_cliente) == true && (compare == true)){ //usuario correcto es decir es unico que lo encientre y que sea el unice n la base de datos
        const user = {
            rut_cliente,
            clave_cliente
        }
        jwt.sign({ user },'llavesecreta', (err,token) => {
            res.json({
                status: 1,
                token
            })
        })
    }else{
        res.json({
            status:-1
        });
    }*/
};

module.exports = {
    login
};