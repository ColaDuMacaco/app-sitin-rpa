//const { Pool } = require('pg')
const jwt = require('jsonwebtoken');
const { poolrpa } = require('../config/database')
const bcrypt = require('bcrypt');
var AWS = require("aws-sdk");


const clientes = async (req,res) =>{
    const response = await poolrpa.query(`select * from cliente`);
    console.log('Getting Clientes');
    console.log(response.rows[0].rut_cliente);
    res.json(c);
};

const postulaciones = async (req, res) => {
    res.header("Access-Control-Allow-Origin","*");
    const { rut_cliente } = req.body;
    console.log(rut_cliente)
    const sqlQuery = `select 
                            nombre_entidad, id_calificacion, 
                            calificacion.nombre_programa, 
                            calificacion.fecha_inicio_programa, 
                            programa.monto_min_programa,
                            programa.monto_max_programa,
                            programa.nombre_region,
                            programa.fecha_termino_programa 
                        from 
                            calificacion 
                        inner join 
                                entidad on calificacion.id_entidad = entidad.id_entidad
                        inner join 
                            programa on programa.id_programa = calificacion.id_programa
                        where 
                            rut_cliente = $1 and calificacion.admisibilidad like '%Admisibilidad aceptada%' 
                        and 
                            programa.monto_max_programa > 0
                        order by id_calificacion desc`;
    const values = [rut_cliente]
    const response = await poolrpa.query(sqlQuery, values)
    //console.log(response.rows["0"].rut_cliente)
    //console.log(response);
    res.json(response.rows)

};  

//getClientes();
module.exports = {
    clientes, 
    postulaciones,
};