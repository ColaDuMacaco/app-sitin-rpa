//const { Pool } = require('pg')
const { poolrpa } = require('../config/database')

const RegisterEntidad = async (req,res) => {
    res.header("Access-Control-Allow-Origin","*");
    const {rut_cliente, id_entidad, clave_entidad, activo} = req.body 
    const getUserEntidades = `select * from usuario_entidad where rut_cliente = $1 and id_entidad = $2;`
    const values = [rut_cliente,id_entidad];
    const response = await poolrpa.query(getUserEntidades,values);
    if(response.rows.length==0){
        //Insertando entidad
        if(activo == true){
            const sqlQuery = `INSERT INTO usuario_entidad 
                                (rut_cliente, clave_entidad, id_entidad, activo) 
                                values ($1,$2,$3,$4) RETURNING *;`
            const values2 = [rut_cliente,clave_entidad,id_entidad,activo];
            const response2 = await poolrpa.query(sqlQuery,values2);
            res.json({
                status:1
            })
        }else{
            //no se inserta porque el switch esta en off
            res.json({
                status:-1
            })
        }
    }else{
        //cambiando valor de la entidad
        if(clave_entidad == "" && activo == false){
            const alterUser1 = `update usuario_entidad set activo = $1 
                                where rut_cliente = $2 and id_entidad = $3 
                                RETURNING *`;
            const values2= [activo,rut_cliente,id_entidad];
            const response2 = await poolrpa.query(alterUser1,values2);
            res.json({
                status:-2
            })
        }else if(clave_entidad != "" && activo == true){
            const alterUser2 = `update usuario_entidad set activo = $1 
                            where rut_cliente = $2 and clave_entidad = $3 and id_entidad = $4 
                            RETURNING *`;
            const values2 = [activo, rut_cliente, clave_entidad,id_entidad];
            const response2 = await poolrpa.query(alterUser2,values2);
            res.json({
                status:2
            })
        }
        
    }  
};
 
//Obteniendo entidades


const getEntidades = async (req,res) => {
    res.header("Access-Control-Allow-Origin","*");
    const {rut_cliente} = req.body;
    const sqlQuery = `select entidad.id_entidad,entidad.nombre_entidad,usuario_entidad.activo 
                        from entidad,usuario_entidad where rut_cliente=$1 
                        GROUP BY entidad.id_entidad,usuario_entidad.activo`;
    const values = [rut_cliente];
    const response = await poolrpa.query(sqlQuery,values);
    if(response.rows.length==0){
       const sqlQuery2 = `select * from entidad`;
       const response2 = await poolrpa.query(sqlQuery2);
       res.json(response2.rows)
    }else{
        res.json(response.rows)
    }
};
//getClientes();
module.exports = {
    RegisterEntidad,
    getEntidades
};