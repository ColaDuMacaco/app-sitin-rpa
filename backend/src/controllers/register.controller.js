//import {  validate, clean, format, getCheckDigit } from 'rut.js'
const { json } = require('express');
const jwt = require('jsonwebtoken');
const { poolrpa } = require('../config/database')
const bcrypt = require('bcrypt');

var Fn = {
	// Valida el rut con su cadena completa "XXXXXXXX-X"
	validaRut : function (rutCompleto) {
		if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( rutCompleto ))
			return false;
		var tmp 	= rutCompleto.split('-');
		var digv	= tmp[1]; 
		var rut 	= tmp[0];
		if ( digv == 'K' ) digv = 'k' ;
		return (Fn.dv(rut) == digv );
	},
	dv : function(T){
		var M=0,S=1;
		for(;T;T=Math.floor(T/10))
			S=(S+T%10*(9-M++%6))%11;
		return S?S-1:'k';
	}
}

// Uso de la función
//alert( Fn.validaRut('11111111-1') ? 'Valido' : 'inválido');

function ValidatePassword(password){
    const re = /^(?!.*\s)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[~`!@#$%^&*()--+={}\[\]|\\:;"'<>,.?/_₹]).{5,9}$/;
    return re.test(password)
} 

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

function ValidatePhoneNumber(celular){
    const re = /^(\+?56)?(\s?)(0?9)(\s?)[9876543]\d{7}$/;
    return re.test(String(celular))
};

const register = async (req,res) => {
    console.log(ValidatePassword('12345678'))
    console.log("entrando a funcion de registro")
    res.header("Access-Control-Allow-Origin","*");
    const { rut_cliente, nombre_cliente, apellido_cliente, correo_cliente, clave_cliente, celular } = req.body;
    if(Fn.validaRut(rut_cliente) == true && validateEmail(correo_cliente) == true && ValidatePhoneNumber(celular) == true && ValidatePassword(clave_cliente) == true){
        console.log(ValidatePhoneNumber(celular))
        const sqlQuery = 'INSERT INTO Cliente (rut_cliente, nombre_cliente, apellido_cliente, correo_cliente, clave_cliente,celular) values ($1,$2,$3,$4,$5,$6) RETURNING *';
        const hash = bcrypt.hashSync(clave_cliente, 5);
        const values = [rut_cliente, nombre_cliente, apellido_cliente, correo_cliente, hash,"+56"+celular];
        const response = await poolrpa.query(sqlQuery, values);
        console.log("Registrando clientes con los datos: \n",response.rows);
        res.json({
            status:1
        })
        
    }else if(Fn.validaRut(rut_cliente) == false && validateEmail(correo_cliente) == false && ValidatePhoneNumber(celular) == false && length(celular)==9){
        console.log('ERROR en registro credenciales no validas')
        res.json({
            status:0
        })
        //console.log(res.json(0))

    }else if (Fn.validaRut(rut_cliente) == false) {
        console.log('ERROR en registro en rut')
        res.json({
            status:-1
        })
        //console.log(res.json(-1))
    }

    else if (validateEmail(correo_cliente) == false) {
        console.log('ERROR en registro en mail')
        res.json({
            status:-2
        })
        //console.log(res.json(-2))
    }
    else if (ValidatePhoneNumber(celular) == false) {
        console.log('ERROR en registro en numero de celular')
        res.json({
            status:-3
        })
        //console.log(res.json(-2))
    }else if(ValidatePassword(clave_cliente) == false){
        res.json({
            status:-4
        })
    }
};

const VincularEntidad = async (req,res) =>{
    console.log("entrando a funcion de vincular entidad")
    res.header("Access-Control-Allow-Origin","*");
    const { rut_cliente, id_entidad } = req.body;
    const sqlQuery2 = 'INSERT INTO usuario_entidad (rut_cliente,id_entidad,activo,vinculada) values ($1,$2,true,true) RETURNING *';
    const values2 = [rut_cliente,id_entidad];
    const response2 = await poolrpa.query(sqlQuery2, values2);
    res.json(1)
};
const Guardar = async (req,res) =>{
    console.log("entrando a funcion de vincular entidad")
    res.header("Access-Control-Allow-Origin","*");
    const { clave_entidad, id_usuario_entidad } = req.body;
    const sqlQuery2 = 'update usuario_entidad set clave_entidad = $1 where id_usuario_entidad = $2';
    const values2 = [clave_entidad,id_usuario_entidad];
    const response2 = await poolrpa.query(sqlQuery2, values2);
    res.json(1)
};

module.exports = {
    register,
    VincularEntidad,
    Guardar
};