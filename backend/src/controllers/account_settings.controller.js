const { json } = require('express');
const jwt = require('jsonwebtoken');
const { poolrpa } = require('../config/database')
const bcrypt = require('bcrypt');


function ValidatePassword(password){
    const re = /^(?!.*\s)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[~`!@#$%^&*()--+={}\[\]|\\:;"'<>,.?/_₹]).{5,9}$/;
    return re.test(password)
} 

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

function ValidatePhoneNumber(celular){
    const re = /^(\+?56)?(\s?)(0?9)(\s?)[9876543]\d{7}$/;
    return re.test(String(celular))
};


const account_settings = async (req, res) =>{
    const {rut_cliente, nombre_cliente,apellido_cliente, correo_cliente, clave_cliente,celular} = req.body
    const hash = bcrypt.hashSync(clave_cliente, 5);
    if(validateEmail(correo_cliente) == true && ValidatePhoneNumber(celular) == true && ValidatePassword(clave_cliente) == true){
        const response = await poolrpa.query(`UPDATE cliente set nombre_cliente = $1, apellido_cliente = $2, 
                                            correo_cliente = $3, clave_cliente = $4, celular = $5 where rut_cliente = $6`,[
            nombre_cliente,
            apellido_cliente,
            correo_cliente,
            hash,
            "+56"+celular,
            rut_cliente,
            
        ]);
        console.log(response);
        res.json({
            status: 1
        });
    }else if(validateEmail(correo_cliente) == false && ValidatePhoneNumber(celular) == false && length(celular)==9){
        //error en credenciales
        res.json({
            status: 0
        })
    }else if(validateEmail(correo_cliente) == false){
        //error en formato de correo
        res.json({
            status: -1
        })
    }else if(ValidatePhoneNumber(celular) == false){
        //numero de telefono malo o sin formato
        res.json({
            status: -2
        })
    }else if(ValidatePassword(clave_cliente) == false){
        //la clave no sigue el formato
        res.json({
            status: -3
        })
    }
    //console.log(rut_cliente, nombre_cliente, apellido_cliente, correo_cliente, clave_cliente);
    //res.send("Update User");
};


module.exports = {
    account_settings,
};