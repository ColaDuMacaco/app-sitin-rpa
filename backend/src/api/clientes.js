/* IMPORTS */
const { Router } = require('express')
const express = require('express')
const router = express.Router()
const {clientes,postulaciones} = require('../controllers/clientes.controller')
//const router = require('./register')

/* APIS */
router.get('/clientes',clientes)
router.post('/postulaciones',postulaciones)

module.exports = router