const { Router } = require('express')
const express = require('express')
const router = express.Router()
const { register,VincularEntidad,Guardar} = require('../controllers/register.controller')

/* APIS */

router.post('/register',register)
router.post('/VincularEntidad',VincularEntidad)
router.post('/Guardar',Guardar)

module.exports = router