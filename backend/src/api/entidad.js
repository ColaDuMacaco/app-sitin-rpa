const { Router } = require('express')
const express = require('express')
const router = express.Router()
const { RegisterEntidad,getEntidades } = require('../controllers/entidad.controller')


/* APIS */

router.post('/RegisterEntidad',RegisterEntidad)
router.post('/getEntidades',getEntidades)

module.exports = router