const { Router } = require('express')
const express = require('express')
const router = express.Router()
const { account_settings } = require('../controllers/account_settings.controller')

/* APIS */

router.post('/',account_settings)


module.exports = router