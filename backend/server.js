/* IMPORTS */
const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const axios = require('axios')
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const Pool = require('pg').Pool
const jwt = require('jsonwebtoken');
var nodemailer = require("nodemailer"); 
//const { poolrpa } = require('./src/config/database');

//poolrpa => connection

//-------------------------------------

/* CONFIGS */
const poolrpa = new Pool ({
  host: "localhost",
  user: "postgres",
  password: "postgres",
  database: "rpa",
  port: 3000
});

const server = express()
dotenv.config()
server.use(bodyParser.urlencoded({
    extended: true
}));
server.use(bodyParser.json());
server.use(cors())



/* VARIABLES */
//dotenv.config('./src/config/database.js')
var port = process.env.PORT || 4000
var ip = process.env.PORT || 'localhost'

//server.get('/', (req,res) => res.send('<h1>Server en AWS operando</h1>'))

server.use(express.json())
server.use(express.urlencoded({extended:false}))
server.set('view engine','ejs')

let r = (Math.random() + 1).toString(36).substring(7);

let user = {
  rut:"ashjdgasjdhgasjd",
  correo_cliente:"jhondoe@gmail.com",
  clave_cliente:"asdjhaskdjhaskdroieuolt"
}

let JSON_RESPONSE = {
  tokenUser: 'asdasdas',
  rut_cliente: 'asdasdas',
  clave_aux: 'asdasdas'
}
const JWT_SECRET = 'some super secret...'

server.get('/',(req,res) => {
  res.send('Hello World!');
});

server.get('/forgot-password',(req,res,next) =>{
  res.render('forgot-password');
})

server.post('/forgot-password',async (req,res,next) =>{
  res.header("Access-Control-Allow-Origin","*");
  const { rut_cliente } = req.body;
  const sqlQuery = `select * from cliente where rut_cliente = $1`;
  const values = [rut_cliente];
  const response = await poolrpa.query(sqlQuery,values);
  if(response.rows.length == 1){
    user = {
      rut: response.rows[0].rut_cliente,
      correo_cliente: response.rows[0].correo_cliente,
      clave_cliente: response.rows[0].clave_cliente
    }
    //res.json(user)
    //console.log(user)
    const secret = JWT_SECRET + user.password
    const payload = {
      correo_cliente:user.correo_cliente,
      rut_cliente:user.rut_cliente,
      clave_aux: (Math.random() + 1).toString(36).substring(7)
    };
    const token = jwt.sign(payload,secret,{expiresIn:'5m'});
    const payload2 = jwt.verify(token, secret)
    console.log(payload2)
    //res.json(payload)
    JSON_RESPONSE = {
      tokenUser: token,
      rut_cliente: user.rut,
      clave_aux: payload.clave_aux
    }
    //se te ha enviado al correo tu nueva clave para reset
    console.log('se ha enviado el codigo a tu correo')
    res.json(JSON_RESPONSE);
    var smtTransport = nodemailer.createTransport({
      host:"smtp.ethereal.email",
      post: 587,
      secure: false,
      auth:{
        user: 'emmie.hilll6@ethereal.email',
        pass: "2bg4x55YjuAM7XNMAT"
      }
    })
    var mailOptions = {
      from: "Remitente",
      to: "jfernandezvizcarra@gmail.com",
      subject: "Hey "+response.rows[0].nombre_cliente+" "+response.rows[0].apellido_cliente,
      text: "Tu clave temporal es la siguiente: "+ JSON_RESPONSE.clave_aux + "\n"+":D ..."
    }
    smtTransport.sendMail(mailOptions, (error, info) => {
      if(error){
        res.status(500).send(error.message);
      }else{
        console.log("Email enviado.")
        res.status(200).json(req.body);
      }
    });
    

  }

  //console.log(response.rows[0].correo_cliente)
  //res.send(email);
  //verificar que el usuario exista dentro de la base de datos
  /*if(email != user.email){
    //usuario no existente
    res.json({
      status:-1
    });
    return;
  }else{
    res.send("existe");
  }
  const secret = JWT_SECRET + user.password
  const payload = {
    email: user.email,
    id: user.id
  }
  const token = jwt.sign(payload,secret,{expiresIn:'5m'})
  const link = `http://localhost:4000/reset-password/${user.id}/${token}`
  console.log(link)
  res.send('Password reset link has been sent to ur email...')*/

});

/*server.get('/reset-password/:id/:token',(req,res,next) =>{
  //res.render('forgot-password');
  const {id,token} = req.params;
  //res.send(req.params);
  //check if this id exist in database
  if(id !== user.id){
    res.send('Invalid id...');
    return;
  }
  //We have a valid id, and we have a valid user with this id
  const secret = JWT_SECRET + user.password
  try {
    const payload = jwt.verify(token, secret)
    res.render('reset-password',{email:user.email})
  } catch (error) {
    console.log(error.message)
    res.send(error.message)
  }

});

server.post('/reset-password/:id/:token',(req,res,next) =>{
  //res.send(user)
  const {id,token} = req.params;
  const { password, password2 } = req.body;
  if(id !== user.id){
    res.send('Invalid id...');
    return;
  }
  const secret = JWT_SECRET + user.password
  try {
    const payload = jwt.verify(token, secret) 
    //valdate password and password2 should match
    //we can simply find rthe user with the payload email and id and finally update with new password
    //always hash the password before saving

    user.password = password
    res.send(user)

  } catch (error) {
    console.log(error.message)
    res.send(error.message)
  }

});*/
/* PORTS */

server.use('/api/clientes', require('./src/api/clientes'))
server.use('/api/login', require('./src/api/login'))
server.use('/api/configuracion', require('./src/api/account_settings'))
server.use('/api/RegisterEntidad', require('./src/api/entidad'))
server.use('/api/getEntidades', require('./src/api/entidad'))
server.use('/api/changeState', require('./src/api/account_settings'))
server.use('/api/register', require('./src/api/register'))


/* SERVER */
//----------------------TRABAJO CON ALE------------------------------------
server.post('/api/validar', (req,res,next) => {
  const { rut_cliente, clave_cliente } = req.body;
  var JobStarted = new Boolean(false);
  var JobID = 0;
  let access = ""//"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlJUTkVOMEl5T1RWQk1UZEVRVEEzUlRZNE16UkJPVU00UVRRM016TXlSalUzUmpnMk4wSTBPQSJ9.eyJodHRwczovL3VpcGF0aC9lbWFpbCI6ImNyaXN0aWFuLmNvbmNoYUBzaXRpbi5jbCIsImh0dHBzOi8vdWlwYXRoL2VtYWlsX3ZlcmlmaWVkIjp0cnVlLCJpc3MiOiJodHRwczovL2FjY291bnQudWlwYXRoLmNvbS8iLCJzdWIiOiJhdXRoMHw2MWNlMzc1NGY1NWY3YzAwNjkyMDM2MzQiLCJhdWQiOlsiaHR0cHM6Ly9vcmNoZXN0cmF0b3IuY2xvdWQudWlwYXRoLmNvbSIsImh0dHBzOi8vdWlwYXRoLmV1LmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE2NDA5MTcwMDcsImV4cCI6MTY0MTAwMzQwNywiYXpwIjoiOERFdjFBTU5YY3pXM3k0VTE1TEwzallmNjJqSzkzbjUiLCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIG9mZmxpbmVfYWNjZXNzIn0.q_sBAuReAOW46auf6VuakmfKJKJ4lhC-3mAxR7yNODaFH8QWxm7dqgRSztsOQI9WV5wxnRjfSC2NGwomlKDZIBKdD5nM-NIbZxJVmcVKSkJtF9BItWA5WFDuVq8VVltTH0WkulxRFhuj6bNW_GW0NeXUeP92k-moSgl8QAyAdNvozUmVEJDMrIMjpeWDai0ywx5lC901_tdMlkuI-T17kGlsxUPfF2t-cVSUmH1l7aDWZwQm1mKmXajiztun-KkLKMxmmT9XrCafCNLREP8evi2huJ1rDI72ZiCeos55paTaKZ60r4V2W1w0ev3JT2og7lrDgaynwYaTOgNTJJzZng"
  var stateVal="run";

  // GET ORCHESTRATOR AUTHORIZE KEY
  var request = new XMLHttpRequest();
  request.open("POST","https://account.uipath.com/oauth/token",true);
  request.setRequestHeader("Content-Type", "application/json");
  //request.setRequestHeader("X-UIPATH-TenantName", "");
  var post_data = JSON.stringify({
        'grant_type' : 'refresh_token',
        'client_id': '8DEv1AMNXczW3y4U15LL3jYf62jK93n5',
      'refresh_token': 'WKqjhJC9kCH6ez_1AQ2T3Qu5fVxRnQic1M0LB8PjapVuA'
    });
  request.send(post_data);
  request.onload = () => {     
    if (request.status == 200){
      const obj = JSON.parse(request.responseText);
      access = obj.access_token;
      console.log("Access Token:"+access);     
      CallUiPath()
      }else{
        console.log("Error");
    }
  }

  function CallUiPath() {
    //const JobID1=0;
    const param1 = rut_cliente;
    const param2 = clave_cliente
    var obj2 = {"startInfo":
    { "ReleaseKey":"3c2e33f8-931f-4b49-b647-79d4aaee169f", 
    "Strategy":"ModernJobsCount",
    "JobsCount": '1',
    "InputArguments": "{\"Rut\":\""+param1+"\",\"Pass\":\""+param2+"\"}"}};
    var post_data2 = JSON.stringify(obj2); 

    var request2 = new XMLHttpRequest();
    request2.open("POST","https://cloud.uipath.com/sitinldgbyzw/Sitin/odata/Jobs/UiPath.Server.Configuration.OData.StartJobs",true);
    request2.setRequestHeader("Content-Type", "application/json");
    request2.setRequestHeader("X-UIPATH-OrganizationUnitId", "2848259");
    request2.setRequestHeader("Authorization", "Bearer "+access);
    var post_data2 = JSON.stringify(obj2);      
    request2.send(post_data2);
    request2.onload = () => {     
        if (request2.status == 201){
            const obj2 = JSON.parse(request2.responseText);
          JobID = obj2.value[0].Id;
        //JobID1 = JobID
        console.log("JobID:"+JobID);
        JobStarted = true;
            }else{
                console.log("Error"+request2.responseText);
        }
    };

    if (JobStarted)
    {
        var n = 1;
        while (n < 12) {
          setTimeout(apicall, 5000 * n)
            n++;
        }
    }
  };
  function apicall(){
    if (!stateVal.startsWith("succ"))
    {
      var request3 = new XMLHttpRequest();
      request3.open("GET","https://cloud.uipath.com/sitinldgbyzw/Sitin/orchestrator_/odata/Jobs?$Filter=Id eq "+String(JobID),true);
      request3.setRequestHeader("Content-Type", "application/json");
      request3.setRequestHeader("X-UIPATH-OrganizationUnitId", "2848259");
      request3.setRequestHeader("Authorization", "Bearer "+access);    
      request3.send();
      request3.onload = () =>{    	
        if (request3.status == 200){
          const obj3 = JSON.parse(request3.responseText);
          //console.log(obj3)             			 
      stateVal = String(obj3.value[0].State).toLowerCase();
      if (stateVal.startsWith("succ"))
      {
                console.log("Return Argument: "+obj3.value[0].OutputArguments);
                if(obj3.value[0].OutputArguments == '{"login":"Clave correcta"}'){
                  const sqlQuery = `update usuario_entidad set vinculada = true where usuario_entidad.rut_cliente = $1`;
                  const values = [rut_cliente];
                  const response = poolrpa.query(sqlQuery,values);
                  res.json({
                    status: 1
                  })
                }else{
                  const sqlQuery = `update usuario_entidad set vinculada = false where usuario_entidad.rut_cliente = $1`;
                  const values = [rut_cliente];
                  res.json({
                    status: -1
                  })
                }
                var str2 = String(obj3.value[0].OutputArguments).substring(13);
                str2 = str2.substring(0,str2.length-2);
                
                  
              //SEND RESPONSE BACK TO SLACK with Output ARGument from RObot       
      }
      console.log("State:"+stateVal);
        }else{
          console.log("Error"+request3.responseText);
      }
      }  
    }
    }
  
});


axios.get('https://ip4.seeip.org/')
  .then(function (response) {
    // handle success
    //console.log(response.data);
    server.listen(port, () => {
        console.log(`Servidor AppSitin corriendo en: http://${ip}:${port}.`)
    })
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .then(function () {
    // always executed
  });

