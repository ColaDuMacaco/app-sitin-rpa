## Boost Pyme

```bash
git clone https://ColaDuMacaco@bitbucket.org/ColaDuMacaco/app-sitin-rpa.git
```

---
**Requerimientos de Software**

1. Git 2.17.1@latest [(ver aquí)](https://git-scm.com/downloads).
2. Node js 14.17.0@latest [(ver aquí)](https://nodejs.org/en/).
3. npm 6.14.13@latest (viene incluido con node) [(ver aquí)](https://nodejs.org/en/).
4. Postgresql 13.04@latest [(ver aquí)](https://www.postgresql.org/download/).
5. Expo 4.5.2@latest [(ver aquí)](https://docs.expo.dev/get-started/installation/).
6. UiPath 5.04 [(ver aquí)](https://www.uipath.com).
7. Micorsoft Visual C++ 2015 Redistributable [(ver aquí)](https://www.microsoft.com/en-us/download/details.aspx?id=52685).
8. SO Windows 10 Home o Pro [(ver aquí)](https://www.microsoft.com/es-mx/software-download/windows10).
9. Navegadores para uso de UiPath: Internet Explorer v8.0@latest, Google Chrome v64@latest, Mozilla Firefox v52.0@latest y Micorosoft Edge v18.03@latest.

---
**Requerimientos Físicos**

1. CPU mínimo 2x1.8Ghz *32-bit (x86)* recomendado *4x2.4Ghz 64-bit (x64)*.
2. Ram mínimo 4 Gb recomendado 8 Gb.
3. Espacio en disco Mínimo de 5 Gb recomendado 8 Gb.

** *Tutorial uso de git y comandos básicos* ** 

- [Comandos](https://gitsheet.wtf) 

- [Videos Sugeridos](https://www.youtube.com/watch?v=hWglK8nWh60)
 
---

## Posibles problemas al momento de instalar expo en windows

*Abrir powershell o cmd modo administrador* 
--
```sh
expo : File "C:\Users\your_user\AppData\Roaming\npm\expo.ps1" cannot be loaded because running scripts is disabled on this system. For more 
information, see about_Execution_Policies at https:/go.microsoft.com/fwlink/?LinkID=135170.
At line:1 char:1
+ expo --version
+ ~~~~
    + CategoryInfo          : SecurityError: (:) [], PSSecurityException
    + FullyQualifiedErrorId : UnauthorizedAccess
```

Esto quiere decir que no tiene habilitado los scripts para su ejecución:

Si desea ver el foro donde se hace resolución del problema [pinche aquí](https://stackoverflow.com/questions/60410285/expo-version-command-shows-running-scripts-disabled-on-this-machine)

*Solución*
--
```bash
set-ExecutionPolicy RemoteSigned
presione y/s
```
