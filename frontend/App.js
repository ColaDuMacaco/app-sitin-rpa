//import liraries
import React, {useEffect} from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginStack from './stacks/loginstack';
import MainStack from './stacks/mainstack';
import {Notifications}  from 'expo';
//import * as Permissions from 'expo-permissions';


const Stack = createStackNavigator();


const App = () => {
  /*useEffect(() => {
    registerForPushNotificartion().then(token=>console.log(token))
    .catch(err => console.log(Error))
  },[])*/
  /*const registerForPushNotificartion = async () => {
    const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
  
    if (status !== 'granted'){
      return;
    }
    const token = await Notifications.getExpoPushTokenAsync();
    console.log(token);
    return token;
  };*/
  /*async function registerForPushNotificartion(){
    const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
  
    if (status !== 'granted'){
      return;
    }
    const token = await Notifications.getExpoPushTokenAsync();
    console.log(token);
    return token;
  }*/
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="LoginStack">
        <Stack.Screen
          name="LoginStack"
          component={LoginStack}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MainStack"
          component={MainStack}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2c3e50',
  },
});
export default App;