import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, TextInput, Switch, Alert, FlatList } from "react-native";
import BestButtton from "../loginstack/boton";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Icon from 'react-native-vector-icons/FontAwesome5';
import CustomSwitch from './switch'

const Entidad = () => {


  const API = 'http://192.168.1.21:4000';
  const [data, setdata] = useState([]); 
  const [clave_entidad, setclave_entidad] = useState(true);
  
  useEffect(() => {
    console.log(global.rut_cliente)
    const getEntidades = async () => {
        fetch(API+'/api/getEntidades/getEntidades/', {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+ await AsyncStorage.getItem("token")
           },
        })
            .then(response => response.json())
            .then(async (json) => {
                
              setdata(JSON.parse(JSON.stringify(await json)));
              
            })
            .catch(null)
    };
    getEntidades().catch(null);
  }, []);
  
  const list = () => {
          return data.map((element) => {
            return (

              <View style={styles.container2}
              
              key={element.id_entidad}>
                <View style={styles.item1}>
                  <CustomSwitch nombre_entidad = {element.nombre_entidad} /> 
                </View>
                <View style={styles.item2}>
                  <Text style={styles.Texto}>{element.nombre_entidad}</Text>
                </View>
                <View style={styles.item3}>
                  <TextInput 
                  style={styles.inputText}
                  placeholder="Ingrese su Clave"
                  autoCompleteType="password"
                  secureTextEntry={clave_entidad ? true : false}
                  />
                </View>
              </View>
            );
          });
        };
      return (
          <View style={styles.container} >
              {list()} 

              <View style={styles.boton}>
              <BestButtton  text='GUARDAR' ></BestButtton>
              </View> 
          </View>
        );
  };

  /*return (


Acá voy a dejar el icono del ojo 

<Icon
name={clave_entidad ? 'eye-slash' : 'eye'}
size={15}
color="blue"
onPress={() => setclave_entidad(!clave_entidad)}
/>







  <View style={styles.container} >

    {data.map((datas,i,dataArray) =>(
        <View key={datas.id_usuario_entidad}>
        <View style = {styles.container2}>
        <View style={styles.item1}>   
            <Switch style={styles.switch}
            onValueChange={(toggleValue)=>{
                if(dataArray[i].activo==true){
                    cambiarfalso(dataArray[i].id_usuario_entidad)
                }else{
                    cambiartrue(dataArray[i].id_usuario_entidad)
                }   
            }}
            value={datas.activo}
            />
        </View>
        
        <View style={styles.item2}>
        <Text >{datas.nombre_entidad}</Text>
        </View>
        
        <View style={styles.item3}>
        <TextInput style={styles.inputText} value={datas.clave_entidad}/>
        </View>
        </View>
        </View>
    ))}
    <View style={styles.boton}>
    <BestButtton  text='GUARDAR' ></BestButtton>
    </View> 
</View>
  );*/


const styles = StyleSheet.create({
  container: {        
    backgroundColor:'#e2f4fe',
    flex:1
  },
  container2:{
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    marginHorizontal:'5%',
    marginTop:'5%',
    backgroundColor:'white',
    borderRadius:15
  },
  container3:{
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    marginHorizontal:'5%',
    marginTop:'5%',
    backgroundColor:'grey',
    borderRadius:15
  },
  switch:{
  },

  item1:{
    width:'15%',
    margin:12
  },
  Texto:{
    fontWeight:'bold',
    justifyContent:"center",

  },
  item2:{
    width: '20%',
    marginVertical:12,
    marginLeft:0
  },
  item3:{
    width: '40%',
    margin:12,
  },
  boton:{
    position: 'absolute',
    bottom: 0,
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    marginBottom:'10%',
    paddingHorizontal: '10%'

  },  

  inputText:{
    borderWidth: 1,
    backgroundColor:'white',
    borderColor:'grey'   
  },
});

export default Entidad;