import React, { useState,useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, ScrollView  } from 'react-native';
import {Overlay} from 'react-native-elements'
import {Divider} from 'react-native-elements'
import DataTable from 'react-native-datatable-component';
import AsyncStorage from "@react-native-async-storage/async-storage";
import BestButtton from "../loginstack/boton";
import { Switch, TextInput } from 'react-native-gesture-handler';

const getArticlesFromApi = async () => {
  fetch('https://ip4.seeip.org/json')
  .then(response => response.json())
  .then(data => {
      console.log(data.ip);
  }
  );
};
//entidad programa estado


function format(input){
input.toString();
var num = input.replace(/\./g,'');
if(!isNaN(num)){
num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
num = num.split('').reverse().join('').replace(/^[\.]/,'');
input = num;
}
return input;
};



/*
const Item = ({nombre_entidad,nombre_programa,fecha_inicio_programa,monto_min_programa}) => (
  
  <View>
  <TouchableOpacity style={styles.TouchableOpacity}>
  <View style={styles.container2}>
    
    <View style={styles.item2}>
    
    <View style={styles.container1}>
      <View style={styles.item1}>
        <Text style={styles.entidad}>{nombre_entidad}</Text>
      </View>
      <View style={styles.item1}>
        <Text style={styles.fecha}>{fecha_inicio_programa}</Text>
      </View>
    </View>
    <View>
      <Text style={styles.nombre_programa}>{nombre_programa}</Text>
    </View>
    </View>

    <View style={styles.item3}>
      <Text style={styles.monto}>${monto_min_programa}</Text>
    </View>
    </View>
  </TouchableOpacity>
</View>
  
);
*/
/*
const Item2 = ({nombre_entidad,nombre_programa,fecha_inicio_programa,monto_min_programa}) => (
  
  <View style={styles.container2}>
    <View style={styles.item1}>
      <Text style={styles.entidad}>{nombre_entidad}</Text>
    </View>
    <View style={styles.item1}>
      <Text style={styles.item1}>{nombre_programa}</Text>
    </View>
    <View style={styles.item1}>
      <Text style={styles.item1}>{fecha_inicio_programa}</Text>
    </View>
    <View style={styles.item1}>
      <Text style={styles.monto}>{monto_min_programa}</Text>
    </View>
  </View>
);
*/
// hacer un arreglo con los visibles y asociarlo a los id de los botones, así cuando visible[1] == true implica que el overlay 1 pasa a ser visible
const Nomina = () => {

  /*
  const arregloResumenes = [
    {
      esVisible:false
    },
    {
      esVisible:false
    },
  ];

  const [resumenes, hacerVisible] = useState(arregloResumenes);
  
  const cambiarEstado = () => {
    hacerVisible(!resumenes.esVisible);
  };

  const Up = () =>{
    return(
    <View>
      <Overlay isVisible={arregloResumenes[0]} onBackdropPress={cambiarEstado} overlayStyle={{height:500, width:300, backgroundColor:'white', borderRadius:10 }}>

        <Text>AAAAA</Text>
        <BestButtton text= "Más información" onPress={cambiarEstado}/>
        </Overlay>

      </View>
    );
  };
  */
  const [visible, setVisible] = useState(false);
  
  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const Pop = ({ nombre_entidad, nombre_programa, fecha_inicio_programa, fecha_termino_programa, monto_min_programa, monto_max_programa, id_calificacion, nombre_region}) =>{
    return(
    <View>
      <Overlay isVisible={visible} onBackdropPress={toggleOverlay} overlayStyle={{height:500, width:300, backgroundColor:'white', borderRadius:10 }}>

        <View style={{margin:6}}>
        <Text style={{fontSize: 15, fontWeight:'bold', color:'#027dc5'}}>{id_calificacion}{nombre_entidad}:</Text>
        <Text style={{fontSize: 13, color:'#027dc5'}}>{nombre_programa}</Text>
        </View>
        <ScrollView>
        <Text style={{fontSize:10, color:'grey', textAlign:'center', margin:5, fontWeight:'bold'}}>{nombre_region}</Text>
        <Text></Text>
        
        <Divider style={{margin:5}} />
        
        <Text style={{fontSize:10, color:'grey', textAlign:'center', margin:5, fontWeight:'bold'}}>Plazo postulación</Text>
        <View style={{flex:0.3, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start'}}>
          
          <View style={{ width:'100%', flex:1, alignItems:'center'}}>
            <Text style={{fontSize:9, color:'grey'}}>Inicio:</Text>
            <Text style={{fontSize:9, color:'grey'}}>{fecha_inicio_programa}</Text>
          </View>

          <View style={{ width:'100%', flex:1,alignItems:'center'}}>
            <Text style={{fontSize:8, color:'grey'}}>Termino:</Text>
            <Text style={{fontSize:8, color:'grey'}}>{fecha_termino_programa}</Text>
          </View>
          
        </View>
        <Divider style={{margin:10}} />
        
        <Text style={{fontSize:10, color:'grey', textAlign:'center', margin:5, fontWeight:'bold'}}>Montos</Text>
        <View style={{flex:0.3, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start'}}>
          
          <View style={{ width:'100%', flex:1, alignItems:'center'}}>
            <Text style={{fontSize:8, color:'grey'}}>Monto Mínimo: </Text>
            <Text style={{fontSize:8, color:'grey'}}>${monto_min_programa}</Text>
          </View>

          <View style={{ width:'100%', flex:1,alignItems:'center'}}>
            <Text style={{fontSize:8, color:'grey'}}>Monto Máximo:</Text>
            <Text style={{fontSize:8, color:'grey'}}>{monto_max_programa}</Text>
          </View>
          
        </View>
        

        <Divider style={{margin:10}} />
        <Text style={{textAlign:'center', color:'grey', fontWeight:'bold', margin:10 }}> Resumen </Text>
        
        <Text style={{fontSize: 10, margin: 5}}>
        Para acceder a este instrumento, los emprendedores/as deberán elaborar y postular una
        Idea de Negocio a través de la página www.sercotec.cl previa validación de los requisitos
        de admisibilidad establecidos en el punto 1.5 de las presentes bases de convocatoria.
        </Text>
        <Text style={{fontSize: 10, margin: 5}}>
      
        Permite a los emprendedores/as, cuyas ideas de negocio hayan sido seleccionadas, recibir
        asesoría de parte de un Agente Operador Sercotec2
        , para la formulación técnica y financiera
        de un Plan de Trabajo, junto con la implementación de Acciones de Gestión Empresarial,
        para el desarrollo de competencias y capacidades, e Inversiones, consistentes en la
        adquisición de bienes para cumplir los objetivos del proyecto.
        </Text>
      
        <Text style={{fontSize: 10, margin: 5}}>
      
        El Agente Operador de Sercotec y el Comité de Evaluación Regional evaluarán las Ideas
        de Negocio postuladas que previamente hayan cumplido con los requisitos de admisibilidad
        establecidos en estas Bases, y hayan pasado el puntaje de corte definido por la Dirección
        Regional de Sercotec en función de la disponibilidad presupuestaria.
        </Text>
      
        <Text style={{fontSize: 10, margin: 5}}>
      
        Los/as emprendedores/as que resulten seleccionados, por el Comité de Evaluación
        Regional (CER), deben participar en una Fase de Desarrollo, la cual incluye al inicio una
        etapa de Formulación del Plan de Trabajo, donde el Agente Operador Sercotec apoya al
        emprendedor/a a construir el Plan de Trabajo de la Idea de Negocio seleccionada,
        proponiendo elementos en su estructura técnica y financiamiento, junto al diseño de
        planificación para ejecución del mismo.
        </Text>
      
        <Text style={{fontSize: 10, margin: 5}}>
      
        Posteriormente, se llevarán a cabo el conjunto de actividades necesarias para la
        implementación del Plan de Trabajo; es decir, las Inversiones identificadas y Acciones de
        Gestión Empresarial que derivan de la primera fase. 
        </Text>
      
        
        </ScrollView>
        <BestButtton text= "Más información" onPress={toggleOverlay}/>
        </Overlay>

      </View>
    );
  };
  
  const Item = ({nombre_entidad, nombre_programa, fecha_inicio_programa, fecha_termino_programa, monto_min_programa, monto_max_programa, nombre_region, id_calificacion}) => (
    <View>
    <TouchableOpacity onPress={toggleOverlay}>
    <View style={{backgroundColor:'white', marginVertical:5, padding:5, borderRadius:10, 
        //Sombra
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.42,
        elevation: 2,}}>
      <Text style={{fontWeight:'bold',fontSize:10, color:'#027dc5'}}>{id_calificacion}{nombre_entidad}</Text>
      <Text style={{fontSize:10, color:'#027dc5'}}>{nombre_programa}</Text>
      <View style={styles.container2}>
        <View style={{flex:1, width:'100%',alignItems:'center'}}>
        <Text style={{fontSize:11, fontWeight:'bold', color:'#027dc5'}}>Termino:</Text>
        <Text style={styles.fecha_termino_programa, {fontSize:10, color:'grey'}}>{fecha_termino_programa}</Text>
        </View>
        <View style={{flex:1, width:'100%', alignItems:'center'}}>
        <Text style={{fontSize:11, fontWeight:'bold', color:'#027dc5'}}>Máximo:</Text>
        <Text style={styles.monto_max_programa, {fontSize:10, color:'grey'}}>${monto_max_programa}</Text>
        </View>
      </View>
    </View>

    </TouchableOpacity>
    <View>
    <Pop nombre_entidad={nombre_entidad} nombre_programa={nombre_programa} fecha_inicio_programa={fecha_inicio_programa} fecha_termino_programa={fecha_termino_programa} monto_min_programa={monto_min_programa} monto_max_programa={monto_max_programa} id_calificacion={id_calificacion} nombre_region={nombre_region}/>
    </View>
    </View>
);

  const renderItem = ({item}) => (
    <Item nombre_entidad={item.nombre_entidad} nombre_programa={item.nombre_programa} fecha_inicio_programa={item.fecha_inicio_programa} fecha_termino_programa={item.fecha_termino_programa} monto_min_programa={format(item.monto_min_programa )} monto_max_programa={format(item.monto_max_programa)} id_calificacion={item.id_calificacion} nombre_region={item.nombre_region}/>
   
    );
/*
  const headerList = () => {
    return(
      <View style={styles.titulo}>
        <Text style={styles.text} >Tus ayudas disponibles son:</Text>  
      </View>
    );
  };
  */
  const emptyListMessage = () =>{
    return(
      <Text style={styles.emptyListMessage}> No Tiene Programas a los que Postular</Text>
    );
  };
  const ItemSeparatorView = () => {
    return (
      // Flat List Item Separator
      <View
        style={{
          height: 0.5,
          width: '100%',
          backgroundColor: '#C8C8C8',
        }}
      />
    );
  };

  const API = 'http://'+'192.168.1.21'+':4000';
  const [data, setData] = useState([]);
  const [rut_cliente, setrut] = useState(global.rut_cliente);
  /*
  const columnas = [
    {
      name: 'Nombre programa',
      selector: 'nombre_programa',
      sortable: true
    },
    {
      name: 'Fecha Inicio',
      selector: 'fecha_inicio_programa',
      sortable: true
    },
    {
      name: 'Fecha Fin',
      selector: 'fecha_termino_programa',
      sortable: true
    },
    {
      name: 'Nombre empresa',
      selector: 'nombre_empresa_cliente',
      sortable: true
    },
    {
      name: 'Region',
      selector: 'nombre_region',
      sortable: true
    },
    {
      cell: () =>(
        <BestButtton text='Info'></BestButtton>
      ),
      button: true
    }
  ];
  */
  useEffect(() => {
    
    const API = 'http://'+'192.168.1.21'+':4000';
    const getPostulacion = async () => {
      fetch(API+'/api/clientes/postulaciones', {
        method:'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+await AsyncStorage.getItem("token")
        },
        body: JSON.stringify({
          rut_cliente: rut_cliente
      }),

    })
            .then(response => response.json())
            .then(async (value) => {
                
                setData(value);
                console.log(value)
            });
    }
    getPostulacion().catch(null);
   
  }, [])

    return (

<View style={styles.container}>
  <View style={styles.titulo}>
    <Text style={styles.text}> Tus ayudas disponibles son:</Text>  
  </View>

  <FlatList
    data={data}
    renderItem={renderItem}
    //ListHeaderComponent={headerList}
    ListEmptyComponent={emptyListMessage}
    //ItemSeparatorComponent={ItemSeparatorView} 
    keyExtractor={item => item.id_calificacion.toString()}  
  />

</View>
  );
};
const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor:'#e2f4fe',
      //backgroundColor:'white',
      paddingHorizontal:'5%'
  },
  titulo:{
    marginVertical:15,
    backgroundColor:'white',
    borderRadius:10,
    padding:10
   // fontFamily:'Avenir'
  },
  text:{
    fontWeight:'bold',
    fontSize:20,
    //marginVertical:25,
    color:'#027dc5',
  },
  container1:{
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    backgroundColor:'white',
  },
  TouchableOpacity:{
    backgroundColor:'white',
    borderRadius:10,
    marginVertical:5,
    padding:10,
    //Sombra
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.42,
    
    elevation: 2,
  },
  container2:{
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    marginTop:5
  },
  item1:{
    width:'100%',
    flex:1,
    marginBottom:10
  },
  item2:{
    maxWidth:240,
    width:'100%',
    flex:1,
  },
  item3:{
    width:'100%',
    flex:2,
    position:'absolute',
    //El valor de top deberia de ser 50%, pero no se ven centrados en la aplicacion .-.
    top:'30%',
  },
  tituloColumnas:{
    fontWeight:'bold',
    color:'white'
  },
  emptyListMessage:{

    textAlign:'center'
  },
  monto:{
    fontWeight:'bold',
    color:'#027dc5',
    textAlign:'right',
    paddingRight:10,
  },
  entidad:{
    fontWeight:'bold',
  },
  fecha:{
    color:'#d9dad9',
    paddingStart:3
  },
  Overlay:{
    height:450,
    width:250,
    backgroundColor:'white',
    borderRadius:10
  },
  /////Nuevos Estilos////

});

export default Nomina;