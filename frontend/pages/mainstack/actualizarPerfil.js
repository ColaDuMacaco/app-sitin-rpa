//import liraries
import React, { useState,useEffect }  from 'react';
import { TextInput,StyleSheet,Text, View, Alert, Image, KeyboardAvoidingView, Platform} from 'react-native';
import {Divider} from 'react-native-elements'
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from "@react-native-async-storage/async-storage";
import BestButtton from '../loginstack/boton';
import { NavigationContainer } from '@react-navigation/native';


const API = 'http://'+'192.168.1.21'+':4000';
const actualizar_perfil = ({ navigation }) => {
    const [nombre, setNombre] = useState('');
    const [correo, setcorreo] = useState('');
    const [celular, setcelular] = useState('');
    const [apellido, setapellido] = useState('');
    const [rut, setrut] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);

    //------------- PARA NOTIFICATIONS ---------------------
    const verify = async () => {
        if (!loading) {
            setLoading(true);
            
            console.log("entrando a actualización de perfil")
            fetch(API + '/api/configuracion/', { // la ruta de tu api
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ // aca van lo que pide tu api en body (si no tiene body borra esto)
                    rut_cliente: rut,
                    nombre_cliente: nombre, //antes de 2 puntos el nombre de la variable de la api después la variable almacenada en el frontend
                    apellido_cliente:apellido,
                    clave_cliente: password,
                    correo_cliente: correo,
                    celular:celular,
                }),
            })
                .then((response) => response.json())
                .then(async (json) => {
                    console.log(json);
                    console.log(json.nombre_cliente)
                    if(json.status == '1'){
                        alert("Datos actualizados correctamente");
                        //await sendPushNotification(expoPushToken);
                        //navigation.navigate("");
                      } else if(json.status == '0'){
                        alert("Fallo en la actualización de datos, error en rut, correo y numero telefónico");
                      }
                      else if(json.status == '-1'){
                        alert("Fallo en la actualización de datos, error en correo");
                      }
                      else if(json.status == '-2'){
                        alert("Fallo en la actualización de datos, error en numero de celular");
                      }
                      else if(json.status == '-3'){
                        alert("Fallo en la actualización de datos, la contraseña no cumple con los campos");
                      }
                })
                .catch((error) => {
                    //console.error(error);
                    console.error(error);
                });
            setLoading(false);
        }
    }
    return (
        <View style={styles.container}>        
        <View style={styles.container1}>
            
            <View style={styles.item1}>
            
            <Text style={styles.text}> Nombre (*) </Text>
            <TextInput 
            placeholder="Ej. Nombre"
                style={styles.inputText}
                value={nombre}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                onChangeText={(text) => setNombre(text)}
                
            />
            </View>


            <View style={styles.item1}>
            <Text> Apellido (*) </Text>
            
            <TextInput
            placeholder="Ej. Apellido"
                style={styles.inputText}
                value={apellido}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                onChangeText={(text) => setapellido(text)}
                
            />
            </View>
            

            <View style={styles.item2}>

            <Text> Rut (*) </Text>
            <TextInput
            placeholder="Ej. 11111111-1"
                style={styles.inputText}
                value={rut}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                onChangeText={(text) => setrut(text)}
                
            />
            </View>
            <View style={styles.item2}>
            <Text> Correo (*) </Text>
            <TextInput
            placeholder="Ej. correo@mail.com"
                style={styles.inputText}
                value={correo}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                onChangeText={(text) => setcorreo(text)}
                
            />
            </View>

            <View style={styles.item2}>
            <Text> Clave (*) </Text>
            <TextInput
            placeholder="********"
                style={styles.inputText}
                value={password}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                secureTextEntry={true}
                onChangeText={(text) => setPassword(text)}
                
            />
            </View>
            <View style={styles.item2}>
            <Text> Celular </Text>
            <TextInput
            placeholder="(+569) 98754321"
                style={styles.inputText}
                value={celular}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                onChangeText={(text) => setcelular(text)}
                
            />
            </View>

            <View style={styles.item2}>
             <BestButtton text='Actualizar datos' onPress={/*async () => {verify()}*/ ()=> {navigation.navigate("Mis Postulaciones")} }></BestButtton>    
            </View>

            </View>
            
        </View>
    );
};


async function sendPushNotification(expoPushToken) {
    const message = {
      to: expoPushToken,
      sound: 'default',
      title: 'Original Title',
      body: 'And here is the body!',
      data: { someData: 'goes here' },
    };
  
    await fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(message),
    });
  }

// define your styles
const styles = StyleSheet.create({
    container: {
        flex:1,
        //backgroundColor:'#d7e0eb',
        backgroundColor:'#e2f4fe',
        paddingHorizontal:30,
        paddingBottom:40
    },
    container1:{
        flex:1,
        justifyContent: 'space-between',
        backgroundColor:'white',
        borderRadius:10,
        padding:15,
        //marginVertical:5,
        //Sombra
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },

    logo:{
        justifyContent: 'center',
        alignItems:'center'
    },
    titulo:{
        backgroundColor:'#027dc5',
        padding:'5%',
        borderTopEndRadius:10,
        borderTopStartRadius:10
    },
    textTitulo:{
        textAlign:'center',
        fontWeight:'bold',
        fontSize:25,
        color:'#f8f8f6',
    },
    container2:{
        flex:1,
        backgroundColor:'#f8f8f6',
        borderRadius:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        // La aplicacion manda error porque el paddingHorizontal no tiene un simbolo de '%'
        //paddingHorizontal:'5%',
        
    },
    container3:{
        position: 'absolute',
        bottom: 0,
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        marginBottom:7,
        paddingHorizontal: '3%'
    },
    form:{
        marginBottom:'10%'
    },
    inputText:{
        marginVertical:10,
        marginHorizontal:5,
        height: 40,
        borderWidth: 1,
        padding: 5,
        backgroundColor:'#f8f8f6',
        borderColor:'#f8f8f6',
        borderBottomColor:'#d9dad9',
        
    },
    Divider:{
        margin:10,
    }

});

//make this component available to the app
export default actualizar_perfil;