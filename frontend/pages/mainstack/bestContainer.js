import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, TextInput, Switch, Alert, FlatList } from "react-native";

function BestContainer ({idState, nombre_entidad}) {
    const [isEnabled, setIsEnabled] = useState(false);
    //const [isEnabled, setIsEnabled] = useState(isOn);
    const toggleSwitch = () => setIsEnabled(
        previousState => !previousState
        );
        if(isEnabled == true){

            console.log("True: ",idState," ",isEnabled)
        }
        else{
            console.log("False: ",idState," ",isEnabled)
        }
        //console.log(isEnabled,idState)
        //findState(Switch.arguments(trackColor))
    return (
        
        <View style = {styles.container2} >
            <View style = {styles.item1}>
            <Switch
                id={idState}
                trackColor={{ false: "#767577", true: "#81b0ff"}}
                thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
                ios_backgroundColor="#3e3e3e"
                onValueChange={toggleSwitch}
                value={isEnabled}
            /> 
            </View>
            <View style = {styles.item2}>
                <Text style = {[
                    (isEnabled) ? styles.TextoTrue : styles.TextoFalse    
                ]} > {nombre_entidad} </Text>
            </View>
            <View style = {styles.item3}>
                <TextInput 
                style = {[
                    (isEnabled) ? styles.inputTextTrue : styles.inputTextFalse
                    ]}
                placeholder="Ingrese su Clave"
                autoCompleteType="password"
                editable = {isEnabled}
                selectTextOnFocus = {isEnabled}
                secureTextEntry={true}
                />
            </View>
        </View>
        
    );
  
}
const styles = StyleSheet.create({
    container2:{
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'flex-start',
      marginHorizontal:'5%',
      marginTop:'5%',
      backgroundColor:'white',
      borderRadius:10,

    },
    container3:{
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'flex-start',
      marginHorizontal:'5%',
      marginTop:'5%',
      backgroundColor:'grey',
      borderRadius:15
    },
    switch:{
    },
  
    item1:{
      width:'15%',
      margin:12
    },
    TextoTrue:{
      fontWeight:'bold',
      justifyContent:"center",
  
    },
    TextoFalse:{
        fontWeight:'bold',
        justifyContent:"center",
        color:'grey'
    
      },
    item2:{
      width: '20%',
      marginVertical:12,
      marginLeft:0
    },
    item3:{
      width: '40%',
      margin:12,
    }, 
    inputTextTrue:{
      borderWidth: 1,
      backgroundColor:'white',
      borderColor:'black'   
    }, 
    inputTextFalse:{
      borderWidth: 1,
      borderColor:'grey',
      backgroundColor:'white',  
    },
  });

  
  export default BestContainer;