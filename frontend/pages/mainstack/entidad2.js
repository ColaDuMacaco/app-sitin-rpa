import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, TextInput, Switch, ScrollView   } from "react-native";
import { Button } from "react-native-elements";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Icon from 'react-native-vector-icons/FontAwesome5';
import { CheckBox } from "react-native-elements/dist/checkbox/CheckBox";
//import BestContainer from './bestContainer'


/*
   <Icon
                  reverse={true}
                  reverseColor={[(isEnabled) ? '#d9dad9' : '#027dc5' ]}
                  name='check'
                  type= 'entypo'
                />
*/
const Entidad2 = () => {
  const [habilitado, setHabilitado] = useState(false);
  
  const toggleHabilitado = () => {
    
    setHabilitado(!habilitado);
  };


  const API = 'http://192.168.1.21:4000';
  const [id_entidad, setid_entidad] = useState(0);
  const [clave_entidad, setclave_entidad] = useState("");
  const [activo, setactivo] = useState(false);
  const [data, setdata] = useState([]);
  const [data2, setdata2] = useState({});
  const [loading, setLoading] = useState(false);
  const [rut_cliente, setrut_cliente] = useState(global.rut_cliente);
  var finalArray = []
  var dict = {}
  var dict2 = {}
  var array = [] 
  useEffect(() => {
    console.log(global.rut_cliente)
    const getEntidades = async () => {
        fetch(API+'/api/getEntidades/getEntidades/', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+ await AsyncStorage.getItem("token")
           },
           body: JSON.stringify({
            rut_cliente:rut_cliente,
        }),
        })
            .then(response => response.json())
            .then(async (json) => {
                
              setdata(JSON.parse(JSON.stringify(await json)));
              //console.log(json)
              //console.log(rut_cliente)
              
            })
            .catch(null)
    };
    getEntidades().catch(null);
  }, []);
  function BestContainer ({id_entidad, idState, nombre_entidad,estado,clave_entidad,index}) {

    

    const [clave_entidad1, setclave_entidad1] = useState("");
    dict = {}
    finalArray = []
    if(estado === undefined){
      estado = false
    }
    const [isEnabled, setIsEnabled] = useState(estado);
    //const [isEnabled, setIsEnabled] = useState(isOn);
    const toggleSwitch = () => setIsEnabled(
        previousState => !previousState
        );
        if(isEnabled == false){

            console.log("False: ",idState," ",isEnabled)
            var json = {
              rut_cliente:rut_cliente,
              id_entidad:id_entidad,
              clave_entidad:"",
              estado:isEnabled,
            };
        }
        else{
            console.log("False: ",idState," ",isEnabled)
            var json = {
              rut_cliente:rut_cliente,
              id_entidad:id_entidad,
              clave_entidad:clave_entidad1,
              estado:isEnabled,
            };
        }
        
        array.push(json)
        console.log(array)
        
        //Toca el update
        for (var i = array.length - 1; i >= 0; i--) {
          if(!(array[i]["id_entidad"] in dict)){
            dict[array[i]["id_entidad"]] = (array[i])
            //finalArray.push(array[i])
          }
          //console.log(array[i]);
        }
        console.log('el array')
        //console.log(array)
        console.log('toy pal dict')
        const keys = Object.keys(dict);
        console.log(keys)
        console.log(data)
        /*Object.entries(dict).forEach(([key, value]) => {
          console.log(value.id_entidad,value.rut_cliente);
       });*/
        //console.log(dict)
        //console.log('el final array es:',finalArray)
        //console.log((3).toString())
        //console.log(finalArray["0"].rut_cliente)
        
        //console.log(array)
        
        //findState(Switch.arguments(trackColor))
    return (
        
      <View style={{marginBottom:'5%', backgroundColor:'#f8f8f6', borderRadius:10,shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.20,
      shadowRadius: 1.41,
  
      elevation: 2,}}>
        <View style = {{padding:5,flexDirection: 'row',flexWrap: 'wrap',alignItems: 'flex-start'}} >
            
            <View style = {{width:'100%', flex:0.6,height:50,justifyContent:'center'}}>
              <Switch
                  id={idState}
                  trackColor={{ false: "red", true: "#027dc5" }}
                  thumbColor={isEnabled ? "white" : "white"}
                  ios_backgroundColor="#d9dad9"
                  onValueChange={toggleSwitch}
                  value={isEnabled}
              /> 
            </View>
            
            <View style = {{width:'100%', flex:1,height:50 ,justifyContent:'center',alignItems:'start'}}>
                <Text style = {[
                    (isEnabled) ? styles.TextoTrue : styles.TextoFalse    
                ]} > {nombre_entidad} </Text>
            </View>
            
            <View style = {{width:'100%', flex:1,height:50 ,justifyContent:'center',alignItems:'center', padding:0}}>
            <CheckBox
              
              wrapperStyle={{margin:0, padding:0}}
              checked={isEnabled}
              containerStyle={{margin:0}}
              checkedIcon={
                <Icon
                  name="check-circle"
                  color="green"
                  size={25}
//                  iconStyle={{ marginRight: 10 }}
                />
              }
              uncheckedIcon={
                <Icon
                  name="times-circle"
                  color="grey"
                  size={25}
  //                iconStyle={{ marginRight: 10 }}
                />
              }
              //onPress={() => setCheck1(!check1)}
              />

             
            </View>

        </View>
        <View style = {{padding:5,flexDirection: 'row',flexWrap: 'wrap',alignItems: 'flex-start'}} >
          
          <View style = {{width:'100%', flex:2,height:50,justifyContent:'center', paddingEnd:5}}>
            <TextInput 
              placeholder="Ingrese su Contraseña"
              style = {[
                  (isEnabled) ? styles.inputTextTrue : styles.inputTextFalse
                  ]}
              value={clave_entidad1}
              editable = {isEnabled}
              autoCapitalize="none"
              autoCompleteType="off"
              autoCorrect={false}
              secureTextEntry={true}
              onChangeText={(text) => setclave_entidad1(text)}
              selectTextOnFocus = {isEnabled}
              />
          </View>
            
          <View style = {{width:'100%', flex:1, height:50, justifyContent:'center'}}>
          <Button
          onPress={toggleHabilitado}
                title="Validar"
                //disabled={false}
                loading={habilitado}
                loadingProps={{
                  size: 'small',
                  color: 'rgba(111, 202, 186, 1)',
                }}
                titleStyle={{ 
                  textAlign:'center',
                  color: '#027dc5',
                  fontSize: 12,
                  fontWeight:'bold'
                 }}
                disabledStyle={{
                  borderRadius:5,
                  marginVertical:6,
                  backgroundColor:'#f8f8f6',
                  borderWidth: 1,
                  borderColor:'#027dc5',
                  padding:5                  
                }}
                buttonStyle={{
                  borderRadius:5,
                  marginVertical:6,
                  backgroundColor:'#f8f8f6',
                  borderWidth: 1,
                  borderColor:'#027dc5',
                  padding:5
                }}
                
              />
          </View>

        </View>

      </View>
        
    );
  
};
 const RegisterEntidad = async (dict) => {
    
  const keys = Object.keys(dict);
  //console.log(keys)
  Object.entries(dict).forEach(([key, value]) => {
          //console.log(value.id_entidad,value.rut_cliente);
       
      fetch(API+'/api/RegisterEntidad/RegisterEntidad/', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          //'Authorization': 'bearer '+ await AsyncStorage.getItem("token")
         },
         body: JSON.stringify({
          rut_cliente: rut_cliente,
          id_entidad:value.id_entidad,
          clave_entidad:value.clave_entidad,
          activo:value.estado
      }),
      })
          .then(response => response.json())
          .then(async (json) => { 
            //console.log(json)
            setdata2(JSON.parse(JSON.stringify(await json)));
            //setdata2(json)
            console.log(json)
            //console.log(dict)
            
          })
          .catch(null)
        });
    

};
  
  const list = () => {
    finalArray = []
    for (var i = data.length - 1; i >= 0; i--) {
      if(!(data[i]["id_entidad"] in dict2)){
        dict2[data[i]["id_entidad"]] = (data[i])
        finalArray.push(data[i])
      }

    }
          return finalArray.map((element) => {
            return (
                <View key={element.id_entidad}>
                  <BestContainer id_entidad = {element.id_entidad} idState = {element.nombre_entidad}  nombre_entidad = {element.nombre_entidad} estado = {element.activo} clave_entidad={element.clave_entidad} />
                           
                </View>
            );
          });
        };
      return (
        
          <View style={styles.container} >
        <ScrollView>    
              {list()}
              </ScrollView> 
          
          </View>
        );
        
  };

const styles = StyleSheet.create({
  container: {        
    backgroundColor:'#e2f4fe',
    flex:1,
    padding:20
  },
  container2:{
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    marginTop:'5%',
    backgroundColor:'white',
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,

    elevation: 2,

  },
  container3:{
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    marginHorizontal:'5%',
    marginTop:'5%',
    backgroundColor:'grey',
    borderRadius:10
  },
  switch:{
  },

  item1:{
    width:'15%',
    margin:20
  },
  Texto:{
    fontWeight:'bold',
    justifyContent:"center",

  },
  TextoTrue:{
    fontWeight:'bold',
    fontSize:12,
    color:'#027dc5'

  },
  TextoFalse:{
    fontWeight:'bold',
    justifyContent:"center",
    color:'grey',
    fontSize:12

  },
  item2:{
    width: '20%',
    marginVertical:20,
    marginLeft:0,

  },
  item3:{
    width: '40%',
    margin:20,
  },
  boton:{
    position: 'absolute',
    bottom: 0,
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    marginBottom:'10%',
    paddingHorizontal: '10%'

  },  

  inputText:{
    borderWidth: 1,
    backgroundColor:'white',
    borderColor:'white',
    borderBottomColor:'grey'   
  },
  inputTextTrue:{
    borderWidth: 1,
    backgroundColor:'#f8f8f6',
    borderColor:'#f8f8f6',
    borderBottomColor:'grey'   
  }, 
  inputTextFalse:{
    borderWidth: 1,
    borderColor:'#f8f8f6',
    borderBottomColor:'grey',
    backgroundColor:'#f8f8f6',  
  },
});

export default Entidad2;