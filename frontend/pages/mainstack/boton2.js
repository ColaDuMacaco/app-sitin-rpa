import React from 'react';
import {StyleSheet,TouchableOpacity,Text,View} from 'react-native';

export default function BestButttonSmall({text,onPress}){

    return(

    <TouchableOpacity onPress={onPress}>
        <View style={styles.button}>
            <Text style={styles.buttonText}>{text}</Text>
        </View>
    </TouchableOpacity>
    )
}
const styles= StyleSheet.create({
    button:{
        borderRadius:5,
        marginVertical:6,
        backgroundColor:'#f8f8f6',
        borderWidth: 1,
        borderColor: '#027dc5'
    },
    buttonText:{
        textAlign:'center',
        color: '#027dc5',
        fontSize: 16,
        fontWeight:'bold'
    }
})