import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, TextInput, Switch, Alert, FlatList } from "react-native";

function CustomSwitch (idState) {
    const [isEnabled, setIsEnabled] = useState(false);
    //const [isEnabled, setIsEnabled] = useState(isOn);
    const toggleSwitch = () => setIsEnabled(
        previousState => !previousState
        );
        if(isEnabled == true){

            console.log("True: ",idState," ",isEnabled)
        }
        else{
            console.log("False: ",idState," ",isEnabled)
        }
        //console.log(isEnabled,idState)
        //findState(Switch.arguments(trackColor))
    return (
        
            <Switch
                    id={idState}
                    trackColor={{ false: "white", true: "white" }}
                    thumbColor={isEnabled ? "#027dc5" : "grey"}
                    ios_backgroundColor="#027dc5"
                    onValueChange={toggleSwitch}
                    value={isEnabled}
            /> 
    );
  }
  
  export default CustomSwitch;