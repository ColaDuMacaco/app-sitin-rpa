import React from 'react';
import {
  StyleSheet,
  Image,
  SafeAreaView
} from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList

} from '@react-navigation/drawer';
export default function CustomDrawerContent(props) {
    return (
    <DrawerContentScrollView {...props}>
     <SafeAreaView style={{flex: 1}}>
      {/*Top Large Image */}
      <Image
        source={require('../loginstack/boost_pyme.png')}
        style={styles.sideMenuProfileIcon}
      />
      </SafeAreaView>
      <DrawerItemList {...props} />
    </DrawerContentScrollView>
    );
  }
  const styles = StyleSheet.create({
    sideMenuProfileIcon: {
      resizeMode: 'center',
      width: 100,
      height: 100,
      borderRadius: 100 / 2,
      alignSelf: 'center',
    },
    iconStyle: {
      width: 15,
      height: 15,
      marginHorizontal: 5,
    },
    customItem: {
      padding: 16,
      flexDirection: 'row',
      alignItems: 'center',
    },
  });