//import liraries
import React from 'react';
import { View, StyleSheet} from 'react-native';
import image1 from './boost_pyme.png'
import BestButtton from './boton';
//const image = {uri: "https://raw.githubusercontent.com/ColaDuMacaco/TICs-WarmUp/main/Maqueta/mikasa.png"};

// create a component
const main = ({navigation}) => {
    return (
        <View style={{
            textAlign: 'center',
            backgroundColor: '#e2f4fe'

        }}>
            
            <img style={{
                width: 250,
                height: 250,
                paddingTop: 25,
            }} src={image1}></img>
            <h1 style={{
                fontStyle: 'normal',
                fontFamily:'Roboto',
                fontWeight: 1,
                lineHeight: 1,
                writingDirection:'ltr',
                paddingTop: 10
            }}>Ingresar</h1>
        <View style={styles.container}>
            <View style={styles.button2}>
                <BestButtton  text='Login' onPress={() => navigation.navigate("Login")}></BestButtton>
            </View>
            <View style={styles.button2}>
                <BestButtton text='Registrarse' onPress={() => navigation.navigate("Registro")}></BestButtton>    
            </View>
        </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
        paddingTop: 150,
        marginHorizontal: 100,
        paddingHorizontal:1,
       

    },
    profileImgContainer: {
        height: 100,
        width: 100,
        margin: 100
      },
    letras:{
        fontFamily: 'Helvetica, sans-serif',
        fontWeight:"bold"

    }
});

//make this component available to the app
export default main;
