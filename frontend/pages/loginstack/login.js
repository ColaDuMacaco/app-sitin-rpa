//import liraries
import React, { useState,useEffect }  from 'react';
import { TextInput,StyleSheet,Text, View, Alert, Image, KeyboardAvoidingView, Platform, TouchableWithoutFeedback, Keyboard} from 'react-native';
import { Button } from 'react-native-elements/dist/buttons/Button';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { Overlay } from 'react-native-elements/dist/overlay/Overlay';
import {Divider} from 'react-native-elements'
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from "@react-native-async-storage/async-storage";
import image1 from './boost_pyme.png'
import BestButtton from './boton';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { color } from 'react-native-elements/dist/helpers';
import formatRut from '../funciones/formatRut';


const API = 'http://'+'192.168.1.21'+':4000'; // aca la importe, pero la puedes escribir a mano abajo xd
const Stack2 = createStackNavigator();
const Bienvenido = ({ navigation }) => {

    const [rut_cliente, setrut_cliente] = useState('');
    const [clave_cliente, setclave_cliente] = useState('');
    const [loading, setLoading] = useState(false);
    const [userToken, setUserToken] = React.useState(null);
    const verify = async () => {
    //console.log("1234");
    if (!loading) {

            console.log("verify");
            setLoading(true);
            console.log("antes del fetch");
            fetch(API + '/api/login/login', { // la ruta de tu api
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ // aca van lo que pide tu api en body (si no tiene body borra esto)
                    rut_cliente: rut_cliente, //antes de 2 puntos el nombre de la variable de la api después la variable almacenada en el frontend
                    clave_cliente: clave_cliente 
                    //para transformar JSON.parse(string) de string a json
                }),
                
            })
                .then((response) => response.json())
                .then(async (json) => {
                    console.log(json.token);
                    if (json.status == '1') { 
                        await AsyncStorage.setItem("token",json.token);//llave y resultado
                        console.log(rut_cliente)
                        global.rut_cliente = rut_cliente
                        setLoading(false);
                        navigation.navigate("MainStack");
                    }
                    else{ 
                        setLoading(false);
                        alert('Usuario y/o Contraseña inválida');
                    }
                })
                .catch((error) => {
                    console.error(error);
                    setLoading(false);
                });
            setLoading(false);
        }
    }
    return (

<KeyboardAvoidingView
style = {styles.container}
behavior = {(Platform.OS === 'ios') ? 'padding':'height'}
>
<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
    <View style={styles.inner}>    
    

        
        <View style={styles.container2}>
        <View style={styles.logo}>
        <Image
            style={{
            width: 180,
            height: 180,
        }}
        source={image1}>
        </Image>
        </View >
            <View style={styles.titulo}>
            <Text style={styles.textTitulo} >Ingresar</Text>
            </View>
            <View style={styles.container3}>
            <View style={styles.form}>
            <TextInput
                placeholder="RUT"
                style={styles.inputText}
                maxLength = {12}
                //keyboardType='numeric'
                //value={rut_cliente}
                value={rut_cliente}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                onChangeText={(text) => setrut_cliente(text)}
            />
            
            <TextInput
                placeholder="Contraseña"
                style={styles.inputText}
                value={clave_cliente}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                secureTextEntry={true}
                onChangeText={(text) => setclave_cliente(text)}
            />
            <View>
                <TouchableOpacity onPress={() => {navigation.navigate("Recuperar Contraseña")}}>
                    <Text style={styles.recuperar} >¿Olvidó su Contraseña?</Text>
                </TouchableOpacity>
            </View>
            </View>

            <BestButtton text='Iniciar Sesión' onPress={() => {/*navigation.navigate("MainStack")}*/verify()}}></BestButtton>

            <Divider style={styles.Divider} />
            
            <TouchableOpacity style={{ alignItems:'center'}} onPress={() => {navigation.navigate("Registro - Personal")}}>
                <Text style={{color:'#027dc5'}} >¿No tiene una cuenta? Regístrate aquí</Text>
            </TouchableOpacity>
        

            </View>
        </View>
        
        </View>
</TouchableWithoutFeedback>
</KeyboardAvoidingView>            
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex:1,
        //backgroundColor:'#d7e0eb',
        backgroundColor:'#e2f4fe',
    },
    inner:{
        flex:1,
        padding:30,
        paddingTop:5
    },
    logo:{
        //backgroundColor:'red',
        //flex:1,
        justifyContent: 'center',
        alignItems:'center'
    },
    titulo:{
        //backgroundColor:'#027dc5',
        //flex:1,
        padding:'5%',
        borderTopEndRadius:10,
        borderTopStartRadius:10
    },
    textTitulo:{
        textAlign:'center',
        fontWeight:'bold',
        fontSize:22,
        color:'#027dc5',
    },
    container2:{
        flex:1,
        justifyContent:'space-between',
        backgroundColor:'#f8f8f6',
        borderRadius:10,
        //Sombra
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        // La aplicacion manda error porque el paddingHorizontal no tiene un simbolo de '%'
        //paddingHorizontal:'5%',
        
    },
    container3:{
        //position: 'absolute',
        bottom: 0,
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        marginBottom:7,
        paddingHorizontal: '3%'
    },
    form:{
        marginBottom:'5%'
    },
    inputText:{
        marginVertical:10,
        marginHorizontal:5,
        height: 40,
        borderWidth: 1,
        padding: 5,
        backgroundColor:'#f8f8f6',
        borderColor:'#f8f8f6',
        borderBottomColor:'#d9dad9',
        
    },
    Divider:{
        margin:10,
    },
    recuperar:{
        textAlign:'right',
        color:'#027dc5'
    },


});

//make this component available to the app
export default Bienvenido;