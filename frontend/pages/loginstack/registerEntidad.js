import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, TextInput,Switch,Alert } from "react-native";
import BestButtton from "../loginstack/boton";
import AsyncStorage from "@react-native-async-storage/async-storage";

const IngreseClave = (props) => {
  
  return (
    <TextInput
      {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
      IngreseClave
      maxLength={25}
      secureTextEntry={true}
      autoCapitalize="none"
      autoCompleteType="off"
    />
  );
}


const ObtenerEntidades = async () => {

  const API = 'http://192.168.1.21:4000';
  const [data, setdata] = useState([]);
  const [isEnabled, setIsEnabled] = useState(false);
  const [value, onChangeText] = React.useState('Ingrese clave de Entidad');
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);
  fetch(API+'/api/getEntidades/getEntidades/', {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'bearer '+await AsyncStorage.getItem("token")
    },
  })
  .then(response => response.json())
  .then(value => {
                
    setdata(value);
    console.log(value);
    
  })
  .catch(null);
  

  const list = () => {
    return data.map((element) => {
      return (
        <View key={element.id_entidad} style={{margin: 10}}>
          <Text>{element.nombre_entidad}</Text>
          <IngreseClave
          multiline
          numberOfLines={4}
          value={value}
          onChangeText={text => onChangeText(text)}
          style={{padding: 10}}
        />
          <Switch
            trackColor={{ false: "#767577", true: "#81b0ff" }}
            thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
            ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
          
        </View>
      );
    });
  };

  return (
  <View>
        {list()} 

  </View>
    );
  /*return (

    <View
    style={{
      backgroundColor: value,
      borderBottomColor: '#000000',
      borderBottomWidth: 1,
    }}>
      
      <UselessTextInput
          multiline
          numberOfLines={4}
          onChangeText={text => onChangeText(text)}
          value={value}
          style={{padding: 10}}
        />
      <Switch
        trackColor={{ false: "#767577", true: "#81b0ff" }}
        thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch}
        value={isEnabled}
      />
      
    </View>
  )*/
}; 



const registerEntidad = () => {
  const API = 'http://192.168.1.21:4000';
  const [data, setdata] = useState([]);
  const [rut_cliente, setrut] = useState(global.rut_cliente);
  const [id_entidad, setid] = useState(global.id_entidad);
};




























/*const Entidad = () => {

const API = 'http://localhost:4000';


  const [data, setdata] = useState([]);
  const [data2, setdata2] = useState([]);
  const [refreshKey, setRefreshKey] = useState(0);
  const [rut_cliente, setrut] = useState(global.rut_cliente);

  useEffect(() => {
    console.log(global.rut_cliente)
    const getEntidades = async () => {
        console.log("poder1")
        fetch('http://localhost:4000/api/entidadVinculada/entidad/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+await AsyncStorage.getItem("token")
      },
      body: JSON.stringify({
          rut_cliente: rut_cliente
        
      }),
    })
            .then(response => response.json())
            .then(value => {
                
                setdata(value);
                console.log(value)
                
                
            });
    };
    getEntidades().catch(null);



    const getNoEntidades = async () => {
        fetch('http://localhost:4000/api/entidadNoVinculada/noentidad/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+await AsyncStorage.getItem("token")
      },
      body: JSON.stringify({
          rut_cliente: rut_cliente
        
      }),
    })
            .then(response => response.json())
            .then(value => {
                
                setdata2(value);
                console.log(value)
                
                
            });
    };
    getNoEntidades().catch(null);


}, [refreshKey]);


const cambiarfalso = async (id) => {
    fetch('http://localhost:4000/api/changeState/configuracionEstadoFalse/', {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'Authorization': 'bearer '+await AsyncStorage.getItem("token")
  },
  body: JSON.stringify({
        id_usuario_entidad: id
    
  }),
})
        .then(response => response.json())
        .then(value => {
            
            console.log(value)
            setRefreshKey(oldKey => oldKey +1)
            
            
        });
};
const cambiartrue = async (id) => {
    fetch('http://localhost:4000/api/changeState/configuracionEstadoTrue/', {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'Authorization': 'bearer '+await AsyncStorage.getItem("token")
  },
  body: JSON.stringify({
    id_usuario_entidad: id
    
  }),
})
        .then(response => response.json())
        .then(value => {
            
            console.log(value)
            setRefreshKey(oldKey => oldKey +1)
            
            
        });
};

const insertar = async (id) => {
  fetch('http://localhost:4000/api/register/VincularEntidad', {
method: 'POST',
headers: {
  Accept: 'application/json',
  'Content-Type': 'application/json',
  'Authorization': 'bearer '+await AsyncStorage.getItem("token")
},
body: JSON.stringify({
  rut_cliente: rut_cliente,
  id_entidad: id,
  
}),
})
      .then(response => response.json())
      .then(value => {
            console.log('Se vincula')
            console.log(value)
            setRefreshKey(oldKey => oldKey +1)
          
          
      });
};

const desvincular = async (id) => {
  fetch('http://localhost:4000/api/register/DesvincularEntidad', {
method: 'POST',
headers: {
  Accept: 'application/json',
  'Content-Type': 'application/json',
  'Authorization': 'bearer '+await AsyncStorage.getItem("token")
},
body: JSON.stringify({
  id_usuario_entidad: id
  
}),
})
      .then(response => response.json())
      .then(value => {
            console.log('desvincula3')
            console.log(value)
            setRefreshKey(oldKey => oldKey +1)
          
          
      });
};

  return (
  <View style={styles.container} >

    {data.map((datas,i,dataArray) =>(
        <View key={datas.id_usuario_entidad}>
        <View style = {styles.container2}>
        <View style={styles.item1}>   
            <Switch style={styles.switch}
            onValueChange={(toggleValue)=>{
                if(dataArray[i].activo==true){
                    cambiarfalso(dataArray[i].id_usuario_entidad)
                }else{
                    cambiartrue(dataArray[i].id_usuario_entidad)
                }   
            }}
            value={datas.activo}
            />
        </View>
        
        <View style={styles.item2}>
        <Text >{datas.nombre_entidad}</Text>
        </View>
        
        <View style={styles.item3}>
        <TextInput style={styles.inputText} value={datas.clave_entidad}/>
        </View>
        </View>
        </View>
    ))}
    <View style={styles.boton}>
    <BestButtton  text='GUARDAR' ></BestButtton>
    </View> 
</View>
  );
};*/

const styles = StyleSheet.create({
  container: {        
    backgroundColor:'#e2f4fe'
  },
  inputText:{
    height: 25,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    backgroundColor: 'white',
    borderColor:'grey'
  },
  container2:{
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    marginHorizontal:'10%',
    marginTop:'5%'
  },
  switch:{
  },

  item1:{
    width: '10%',
    margin:12
  },
  item2:{
    width: '10%',
    margin:12,
    marginLeft:0
  },
  item3:{
    width: '60%',
  },
  boton:{
    marginBottom:'10%'
  },  
  Texto:{
        justifyContent:"center",
        textAlign:'center',
        height: 12,
        width: 12,
        fontWeight:"900",
        textDecorationStyle:"solid",
        fontSize:40
    },
    Input:{
        borderWidth: 1
    },
    itemg: {
        flex:1,
        justifyContent:"center",
        padding:5,
        marginVertical:5,
        paddingHorizontal: '20%'
    },
    itemr: {
        justifyContent:"center",
        padding:5,
        marginVertical:5,
    },
    consulta: {
      flexDirection: "row",
      margin: 20,
      backgroundColor: "white",
      borderRadius: 20,
      padding: 35,

      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5

    }
});

export default ObtenerEntidades;