import React from 'react';
import { View,StyleSheet} from 'react-native';
//import './registro.css';
import image1 from './boost_pyme.png'

const Logo = ({ navigation }) => {
    return(
        <View>
            <img style={styles.imagen}src={image1}></img>
        </View>
    );

    const styles = StyleSheet.create({
        container: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'white',
        },
        inputText:{
            border:'solid',
            borderstyle:'solid'
        },
        imagen:{
            width: '50px',
            height: '50px'
      
        }
    });
}