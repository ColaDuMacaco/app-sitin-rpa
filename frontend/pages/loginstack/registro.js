//import liraries
import React, { useState,useEffect,useRef } from 'react';
import { View, Text, StyleSheet,TextInput,Platform, StatusBar} from 'react-native';
//import './registro.css';
import BestButtton from './boton';
import * as Notifications from 'expo-notifications'
import Constants from 'expo-constants';

Notifications.setNotificationHandler({
    handleNotification: async () => ({
      shouldShowAlert: true,
      shouldPlaySound: true,
      shouldSetBadge: false,
    }),
  });

// create a component
const API = 'http://192.168.1.21:4000'; // aca la importe, pero la puedes escribir a mano abajo xd
//nombre, contrasena, correo, direccion y rut
const Registro = ({ navigation }) => {
    const [nombre, setNombre] = useState('');
    const [correo, setcorreo] = useState('');
    const [celular, setcelular] = useState('');
    const [apellido, setapellido] = useState('');
    const [rut, setrut] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);

    //------------- PARA NOTIFICATIONS ---------------------
    const [expoPushToken, setExpoPushToken] = useState('');
    const [notification, setNotification] = useState(false);
    const notificationListener = useRef();
    const responseListener = useRef();
    function validateForm() {
        return rut.length > 0 && password.length > 0;
      }
      useEffect(() => {
        registerForPushNotificationsAsync().then(token => setExpoPushToken(token));
    
        // This listener is fired whenever a notification is received while the app is foregrounded
        notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
          setNotification(notification);
        });
    
        // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
        responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
          console.log(response);
        });
    
        return () => {
          Notifications.removeNotificationSubscription(notificationListener.current);
          Notifications.removeNotificationSubscription(responseListener.current);
        };
      }, []);
    
    const verify = async () => {
        if (!loading) {
            setLoading(true);
            
            console.log("entrando a registro")
            fetch(API + '/api/register/register', { // la ruta de tu api
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ // aca van lo que pide tu api en body (si no tiene body borra esto)
                    rut_cliente: rut,
                    nombre_cliente: nombre, //antes de 2 puntos el nombre de la variable de la api después la variable almacenada en el frontend
                    apellido_cliente:apellido,
                    clave_cliente: password,
                    correo_cliente: correo,
                    celular:celular,
                }),
            })
                .then((response) => response.json())
                .then(async (json) => {
                    console.log(json);
                    console.log(json.nombre_cliente)
                    if(json.status == '1'){
                        alert("Registrado con exito!");
                        await sendPushNotification(expoPushToken);
                        navigation.navigate("Bienvenido");
                      } else if(json.status == '0'){
                        alert("Fallo el registro :( error en rut, correo y numero telefónico");
                      }
                      else if(json.status == '-1'){
                        alert("Fallo el registro :( error en rut");
                      }
                      else if(json.status == '-2'){
                        alert("Fallo el registro :( error en correo");
                      }
                      else if(json.status == '-3'){
                        alert("Fallo el registro :( error en numero de celular");
                      }
                      else if(json.status == '-4'){
                        alert("Fallo el registro :( la contraseña no cumple con los campos");
                      }
                })
                .catch((error) => {
                    //console.error(error);
                    console.error(error);
                });
            setLoading(false);
        }
    }
    return (
        <View style={styles.container}>
          <View style={styles.container1}>        
        <View style={styles.container2}>
            
            <View style={styles.item1}>
            
            <Text style={styles.text}> Nombre (*) </Text>
            <TextInput 
            placeholder="Ej. Pedro"
                style={styles.inputText}
                value={nombre}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                onChangeText={(text) => setNombre(text)}
                
            />
            </View>


            <View style={styles.item1}>
            <Text> Apellido (*) </Text>
            
            <TextInput
            placeholder="Ej. Plaza"
                style={styles.inputText}
                value={apellido}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                onChangeText={(text) => setapellido(text)}
                
            />
            </View>
            </View>

            <View style={styles.item2}>

            <Text> Rut (*) </Text>
            <TextInput
            placeholder="Ej. 1222333-4"
                style={styles.inputText}
                value={rut}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                onChangeText={(text) => setrut(text)}
                
            />
            </View>
            <View style={styles.item2}>
            <Text> Email (*) </Text>
            <TextInput
            placeholder="Ej. correo@mail.com"
                style={styles.inputText}
                value={correo}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                onChangeText={(text) => setcorreo(text)}
                
            />
            </View>
            <View style={styles.item2}>
            <Text> Repita Email (*) </Text>
            <TextInput
            placeholder="Ej. correo@mail.com"
                style={styles.inputText}
                value={correo}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                onChangeText={(text) => setcorreo(text)}
                
            />
            </View>

            <View style={styles.item2}>
            <Text> Contraseña (*) </Text>
            <TextInput
            placeholder="************"
                style={styles.inputText}
                value={password}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                secureTextEntry={true}
                onChangeText={(text) => setPassword(text)}
                
            />
            </View>
            <View style={styles.item2}>
            <Text> Repita Contraseña </Text>
            <TextInput
            placeholder="************"
                style={styles.inputText}
                value={password}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                secureTextEntry={true}
                onChangeText={(text) => setPassword(text)}
                
            />
            </View>
            <View style={styles.item2}>
            <Text> Celular </Text>
            <TextInput
            placeholder="Ej. (+569) 98754321"
                style={styles.inputText}
                value={celular}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                onChangeText={(text) => setcelular(text)}
                
            />
            </View>

            <View style={styles.item2}>
             <BestButtton text='Continuar' onPress={async () => {verify()}}></BestButtton>    
            </View>

            
            </View>
        </View>
    );
};


async function sendPushNotification(expoPushToken) {
    const message = {
      to: expoPushToken,
      sound: 'default',
      title: 'Original Title',
      body: 'And here is the body!',
      data: { someData: 'goes here' },
    };
  
    await fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(message),
    });
  }
  
  async function registerForPushNotificationsAsync() {
    let token;
    if (Constants.isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      token = (await Notifications.getExpoPushTokenAsync()).data;
      console.log(token);
    } else {
      alert('Must use physical device for Push Notifications');
    }
  
    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }
  
    return token;
  }

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        backgroundColor:'#e2f4fe',
        padding:30
    },
    inputText:{
//      marginVertical:10,
      marginHorizontal:3,
      height: 40,
      borderWidth: 1,
      padding: 5,
      backgroundColor:'#f8f8f7',
      borderColor:'#f8f8f6',
      borderBottomColor:'#d9dad9',
      borderRadius:5   
    },
    lefto:{
        textAlign: 'left'
    },
    container1:{
        flex:1,
        justifyContent: 'space-between',
        backgroundColor:'white',
        borderRadius:10,
        padding:15,
        //marginVertical:5,
        //Sombra
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    container2:{
      flex: 2,
      flexDirection: 'row',
      flexWrap: 'wrap',
    },
    item1:{
        width: '50%',
    },
    item2:{
      flex:2
        //width: '100%'
    },
    text:{
    }
});

//make this component available to the app
export default Registro;