import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import main from '../pages/loginstack/main';
import login from '../pages/loginstack/login';
import registro from '../pages/loginstack/registro';
import recuperar_contraseña from '../pages/loginstack/recuperarpassword';
//import ObtenerEntidades from '../pages/loginstack/registerEntidad';
const Stack = createStackNavigator();
const LoginStack = () => {
  return (
      <Stack.Navigator initialRouteName="Bienvenido">
        <Stack.Screen
          name="Main"
          component={main}
        />
        <Stack.Screen
          name="Registro - Personal"
          component={registro}
        />
        <Stack.Screen
          name="Bienvenido"
          component={login}
        />
        <Stack.Screen
          name="Recuperar Contraseña"
          component={recuperar_contraseña}
        />
  
      </Stack.Navigator>
  );
};

export default LoginStack;