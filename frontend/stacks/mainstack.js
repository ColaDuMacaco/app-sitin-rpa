import React from 'react';
import {StyleSheet} from 'react-native';
import Nomina from '../pages/mainstack/nomina';
import Entidad from '../pages/mainstack/entidad';
import Entidad2 from '../pages/mainstack/entidad2';
import login from '../pages/loginstack/login';
import actualizar_perfil from '../pages/mainstack/actualizarPerfil';
import { createDrawerNavigator, DrawerItem, DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Drawer = createDrawerNavigator()

const Mainstack = () => {
  const Logout = async() =>{
    //const navigation1 = useNavigation();
    await AsyncStorage.removeItem("token");
}
  return (
    <Drawer.Navigator initialRouteName="Mis Postulaciones" drawerContent={(props) => {
      return (
      <DrawerContentScrollView {...props}>
      <DrawerItemList {...props}/>
      <DrawerItem label = "Logout" onPress ={()=> {
        Logout()
        props.navigation.navigate(login)
      }}/>
    </DrawerContentScrollView>)
    }}>
    <Drawer.Screen name="Mis Postulaciones" component={Nomina} />
    <Drawer.Screen name="Registro - Entidades" component={Entidad2} />
    <Drawer.Screen name="Actualización de Perfil" component={actualizar_perfil}/>
  </Drawer.Navigator>
  );
};
const styles = StyleSheet.create({
    shadow: {
        shadowColor: '#7F5DF0',
        shadowOffset: {
          width: 0,
          height: 10,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.5,
        elevation: 5
    },
});

export default Mainstack;