# Para la instalación de expo se sugiere usar este comando:
```bash
npm install --global expo-cli
```
*Y en caso de presentarse un problema para ejecutar los scripts es decir que no se encuentren habilitados*

Abrir PowerShell o CMD en modo administrador y ejecutar el siguiente comando...

```bash
set-ExecutionPolicy RemoteSigned
```